import numpy as np
import os, shutil
import sys
import vtk
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
import scipy.io
import matplotlib.pyplot as plt
import imageio
import cv2

# INPUT SECTION
# ===========================================================================================================
# Folder path with input mesh (also used as output folder)
input_folder            = '/outputData/'
patient_names           = ['Patient1']
simulation_data_folder  = '/inputData/Patient1'

displ_reader = vtk.vtkUnstructuredGridReader()
read_img     = vtk.vtkXMLImageDataReader()

zoom_LV_SAX = True
flip_img    = True

for c_pat, patient_name in enumerate(patient_names):
	
	input_image_folder  = input_folder + '/'+patient_name
	results_folder_SAX  = input_folder + '/'+patient_name + '/Phamtom4MRXCAT_SAX/'
	images_for_plots    = input_folder + '/'+patient_name + '/Plots/'
	strain_folder       = input_folder + '/'+patient_name + '/Strain_report/'
	#frame_order_path = simulation_data_folder + patient_name + '/VTK_4_XCAT/Frames_from_ED.txt'

	if not os.path.exists(images_for_plots):
		os.makedirs(images_for_plots)

	mat_file = scipy.io.loadmat(results_folder_SAX+'/Images.mat')
	image4D = mat_file['data']
	n_frames = image4D.shape[-1]

	frames_0 = np.sort(next(os.walk(input_image_folder + '/XCAT_warped_vti/'))[2])
	frames = []
	for frame_sel in frames_0:
	    if frame_sel[0] != '.':
	        if frame_sel[-3:] == 'vti':
	            frames.append(frame_sel)

    # Reading Ref image 0 for dimensions
	if os.path.isfile(input_image_folder+'/XCAT_warped_vti/'+frames[0]):
		read_img.SetFileName(input_image_folder+'/XCAT_warped_vti/'+frames[0])
		read_img.Update()
		img_ref    = read_img.GetOutput()
		spacing    = img_ref.GetSpacing()
		print(spacing)
	else:
		print('Reference XCAT image not available,\nsetting spacing to [0.001,0.001,0.001]')
		spacing = [0.001,0.001,0.001]

	orig_image_SAX     = np.loadtxt(results_folder_SAX+'/Image_new_origin.txt')

	MRI_SAX_plot   = []
	mask_SAX_plot  = []
	displ_SAX_plot = []
	e_r_SAX_plot   = []
	e_c_SAX_plot   = []

	n_MRI = image4D.shape[-1]
	n_frames = np.min([n_MRI,n_frames])
	for frame_sel in range(n_frames):
		print('Frame ',frame_sel)
		vti_image = vtk.vtkImageData()
		vti_image.SetDimensions(image4D.shape[0],image4D.shape[1],image4D.shape[2])
		vti_image.SetSpacing(spacing[0] ,spacing[1],spacing[2])
		vti_image.SetOrigin(orig_image_SAX[0],orig_image_SAX[1],orig_image_SAX[2])
		new_data = np.real(image4D[:,:,:,frame_sel])
		new_shape = [image4D.shape[0],image4D.shape[1],image4D.shape[2]]

		e_radial = np.load(results_folder_SAX+'/SAX_radial_GT_%d.npy' %(frame_sel+1,))
		e_circ   = np.load(results_folder_SAX+'/SAX_circ_GT_%d.npy' %(frame_sel+1,))
		Target_mask_SAX   = np.load(results_folder_SAX+'/SAX_mask_GT_%d.npy' %(frame_sel+1,))

		linear_array = new_data.reshape(-1,1,order='F')
		intensity_vtk = numpy_to_vtk(linear_array)
		intensity_vtk.SetName('labels')
		vti_image.GetPointData().AddArray(intensity_vtk)
		vti_image.Modified() 

		linear_array = e_radial.reshape(-1,1,order='F')
		intensity_vtk = numpy_to_vtk(linear_array)
		intensity_vtk.SetName('e_radial')
		vti_image.GetPointData().AddArray(intensity_vtk)
		vti_image.Modified() 

		linear_array = e_circ.reshape(-1,1,order='F')
		intensity_vtk = numpy_to_vtk(linear_array)
		intensity_vtk.SetName('e_circ')
		vti_image.GetPointData().AddArray(intensity_vtk)
		vti_image.Modified() 

		writer = vtk.vtkXMLImageDataWriter()
		writer.SetFileName(results_folder_SAX+'/Frame_%06d.vti' %(frame_sel+1,))
		writer.SetInputData(vti_image)
		writer.Write()

		off_plane          = 0#new_shape[2]//2#-10

		if patient_name == 'Scar':
			sax_slice_selected = 3
		elif patient_name == 'HCM':
			sax_slice_selected = 5
		elif patient_name == 'NOR':
			sax_slice_selected = 4
		else:
			sax_slice_selected = 3

		MRI_SAX_plot.append(new_data[:,:,sax_slice_selected])
		mask_SAX_plot.append(Target_mask_SAX[:,:,sax_slice_selected])
		e_r_SAX_plot.append(e_radial[:,:,sax_slice_selected])
		e_c_SAX_plot.append(e_circ[:,:,sax_slice_selected])

		if zoom_LV_SAX and frame_sel == 0:
			MRI_SAX_plot_zoom   = []
			mask_SAX_plot_zoom  = []
			e_r_SAX_plot_zoom   = []
			e_c_SAX_plot_zoom   = []

			cropX,cropY = np.where(Target_mask_SAX[:,:,sax_slice_selected]==1)
			# cropX = 100
			# cropY = 100
			# minX = cropX-40; maxX = cropX+40
			# minY = cropY-40; maxY = cropY+40
			minX = np.max([cropX.min()-70,0]); maxX = np.min([cropX.max()+70,new_shape[0]])
			minY = np.max([cropY.min()-70,0]); maxY = np.min([cropY.max()+70,new_shape[1]])
			if (maxX-minX)<100 or (maxY-minY)<100:
				print('Cropped image is too smal, keeping original dimensions')
				minX = 0; maxX = new_shape[0]
				minY = 0; maxY = new_shape[1]
			print('Img size is ',maxX-minX,' x ',maxY-minY)
		if zoom_LV_SAX:
			MRI_SAX_plot_zoom.append(new_data[minX:maxX,minY:maxY,sax_slice_selected])
			mask_SAX_plot_zoom.append(Target_mask_SAX[minX:maxX,minY:maxY,sax_slice_selected])
			e_r_SAX_plot_zoom.append(e_radial[minX:maxX,minY:maxY,sax_slice_selected])
			e_c_SAX_plot_zoom.append(e_circ[minX:maxX,minY:maxY,sax_slice_selected])

	n_images = 8
	freq_sel = np.max([np.floor(n_frames/n_images),1])
	print(freq_sel)
	frames_to_use = np.arange(0,n_frames,freq_sel,dtype=int)
	print(frames_to_use)
	print(frames_to_use[0:n_images])

	np.save(images_for_plots+'/MRI_SAX.npy',np.array(MRI_SAX_plot)[frames_to_use])
	np.save(images_for_plots+'/mask_SAX.npy',np.array(mask_SAX_plot)[frames_to_use])
	np.save(images_for_plots+'/e_r_SAX_plot.npy',np.array(e_r_SAX_plot)[frames_to_use])
	np.save(images_for_plots+'/e_c_SAX_plot.npy',np.array(e_c_SAX_plot)[frames_to_use])
	if zoom_LV_SAX:
		np.save(images_for_plots+'/MRI_SAX_zoom.npy',np.array(MRI_SAX_plot_zoom)[frames_to_use])
		np.save(images_for_plots+'/mask_SAX_zoom.npy',np.array(mask_SAX_plot_zoom)[frames_to_use])
		np.save(images_for_plots+'/e_r_SAX_plot_zoom.npy',np.array(e_r_SAX_plot_zoom)[frames_to_use])
		np.save(images_for_plots+'/e_c_SAX_plot_zoom.npy',np.array(e_c_SAX_plot_zoom)[frames_to_use])

	# Plot images (single row)

	for ijk in range(len(MRI_SAX_plot)):
		if flip_img:
			MRI_SAX_plot[ijk]  = cv2.flip(np.rot90(MRI_SAX_plot[ijk]),0)
			mask_SAX_plot[ijk] = cv2.flip(np.rot90(mask_SAX_plot[ijk]),0)
			e_r_SAX_plot[ijk]  = cv2.flip(np.rot90(e_r_SAX_plot[ijk]),0)
			e_c_SAX_plot[ijk]  = cv2.flip(np.rot90(e_c_SAX_plot[ijk]),0)
		else:
			MRI_SAX_plot[ijk]  = np.rot90(MRI_SAX_plot[ijk])
			mask_SAX_plot[ijk] = np.rot90(mask_SAX_plot[ijk])
			e_r_SAX_plot[ijk]  = np.rot90(e_r_SAX_plot[ijk])
			e_c_SAX_plot[ijk]  = np.rot90(e_c_SAX_plot[ijk])

	MRI_img    = np.concatenate([MRI_SAX_plot[x] for x in frames_to_use],1)# 
	max_MRI    = np.max(MRI_img)
	min_MRI    = np.min(MRI_img)
	msk_img    = np.concatenate([mask_SAX_plot[x] for x in frames_to_use],1)
	radial_img = np.concatenate([e_r_SAX_plot[x] for x in frames_to_use],1)
	circ_img   = np.concatenate([e_c_SAX_plot[x] for x in frames_to_use],1)
	radial_img = np.ma.masked_where(msk_img<1,radial_img)
	circ_img   = np.ma.masked_where(msk_img<1,circ_img)

	plt.close()
	plt.imshow(MRI_img,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
	plt.axis('off')
	plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX.png',bbox_inches='tight', dpi=1200)#,
	plt.close()	

	img2_lines = np.concatenate((MRI_img[:,:MRI_img.shape[1]//2],MRI_img[:,MRI_img.shape[1]//2:]))
	plt.close()
	plt.imshow(img2_lines,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
	plt.axis('off')
	plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_2lines.png',bbox_inches='tight', dpi=1200)#,
	plt.close()	


	# Generate images for single frame visualization
	imageGif = []
	for x in MRI_SAX_plot:
		#imgSel =np.rot90(x)
		imageGif.append(x)
	imageio.mimsave(images_for_plots+'/'+patient_name+'_MRI_SAX_all.gif',imageGif)    

	plt.imshow(MRI_img,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
	iii = np.where(radial_img > 0.8)
	radial_img[iii] = 0.8
	rad_ax2 = plt.imshow(radial_img,cmap=plt.cm.jet,alpha=0.5) #
	plt.axis('off')
	plt.colorbar(rad_ax2,orientation="horizontal", pad=0.2)
	plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_rad_cb.png',bbox_inches='tight',dpi=800)
	plt.close()	

	plt.imshow(MRI_img,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
	circ_ax2 = plt.imshow(circ_img,cmap=plt.cm.jet,alpha=0.5) #
	plt.axis('off')
	plt.colorbar(circ_ax2, orientation="horizontal", pad=0.2)
	plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_circ_cb.png',bbox_inches='tight',dpi=800)
	plt.close()	

	if zoom_LV_SAX:
		for ijk in frames_to_use:
			if flip_img:
				MRI_SAX_plot_zoom[ijk]  = cv2.flip(np.rot90(MRI_SAX_plot_zoom[ijk]),0)
				mask_SAX_plot_zoom[ijk] = cv2.flip(np.rot90(mask_SAX_plot_zoom[ijk]),0)
				e_r_SAX_plot_zoom[ijk]  = cv2.flip(np.rot90(e_r_SAX_plot_zoom[ijk]),0)
				e_c_SAX_plot_zoom[ijk]  = cv2.flip(np.rot90(e_c_SAX_plot_zoom[ijk]),0)
			else:
				MRI_SAX_plot_zoom[ijk]  = np.rot90(MRI_SAX_plot_zoom[ijk])
				mask_SAX_plot_zoom[ijk] = np.rot90(mask_SAX_plot_zoom[ijk])
				e_r_SAX_plot_zoom[ijk]  = np.rot90(e_r_SAX_plot_zoom[ijk])
				e_c_SAX_plot_zoom[ijk]  = np.rot90(e_c_SAX_plot_zoom[ijk])

		# Plot images (single row)
		if len(frames_to_use)%2 == 1:
			frames_to_use=np.concatenate((frames_to_use,[frames_to_use[0]]))
		MRI_img_zoom    = np.concatenate([MRI_SAX_plot_zoom[x] for x in frames_to_use],1)

		msk_img_zoom    = np.concatenate([mask_SAX_plot_zoom[x] for x in frames_to_use],1)
		radial_img_zoom = np.concatenate([e_r_SAX_plot_zoom[x] for x in frames_to_use],1)
		iii = np.where(radial_img_zoom > 0.8)
		radial_img_zoom[iii] = 0.8
		circ_img_zoom   = np.concatenate([e_c_SAX_plot_zoom[x] for x in frames_to_use],1)
		radial_img_zoom = np.ma.masked_where(msk_img_zoom<1,radial_img_zoom)
		circ_img_zoom   = np.ma.masked_where(msk_img_zoom<1,circ_img_zoom)

		msk_img_zoom2lines = np.concatenate((msk_img_zoom[:,:msk_img_zoom.shape[1]//2],msk_img_zoom[:,msk_img_zoom.shape[1]//2:]))

		plt.close()
		plt.imshow(MRI_img_zoom,cmap=plt.cm.gray)#,vmin=max_MRI,vmax=max_MRI)
		plt.axis('off')
		plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX.png',bbox_inches='tight',dpi=800)
		plt.close()	

		img2_lines = np.concatenate((MRI_img_zoom[:,:MRI_img_zoom.shape[1]//2],MRI_img_zoom[:,MRI_img_zoom.shape[1]//2:]))
		plt.imshow(img2_lines,cmap=plt.cm.gray)#,vmin=max_MRI,vmax=max_MRI)
		plt.axis('off')
		plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_2lines.png',bbox_inches='tight',dpi=800)
		plt.close()	

		plt.imshow(MRI_img_zoom,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
		rad_ax2 = plt.imshow(radial_img_zoom,cmap=plt.cm.jet,alpha=0.5) #
		plt.axis('off')
		plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_rad.png',bbox_inches='tight',dpi=800)
		plt.close()	

		strain2lines = np.concatenate((radial_img_zoom[:,:radial_img_zoom.shape[1]//2],radial_img_zoom[:,radial_img_zoom.shape[1]//2:]))
		strain2lines = np.ma.masked_where(msk_img_zoom2lines<1,strain2lines)
		plt.imshow(img2_lines,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
		rad_ax2 = plt.imshow(strain2lines,cmap=plt.cm.jet,alpha=0.5) #
		plt.axis('off')
		plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_rad_2lines.png',bbox_inches='tight',dpi=800)
		plt.close()	

		plt.imshow(MRI_img_zoom,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
		circ_ax2 = plt.imshow(circ_img_zoom,cmap=plt.cm.jet,alpha=0.5) #
		plt.axis('off')
		plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_circ.png',bbox_inches='tight',dpi=800)
		plt.close()	

		strain2lines = np.concatenate((circ_img_zoom[:,:circ_img_zoom.shape[1]//2],circ_img_zoom[:,circ_img_zoom.shape[1]//2:]))
		strain2lines = np.ma.masked_where(msk_img_zoom2lines<1,strain2lines)
		plt.imshow(img2_lines,cmap=plt.cm.gray,vmin=min_MRI,vmax=max_MRI)
		rad_ax2 = plt.imshow(strain2lines,cmap=plt.cm.jet,alpha=0.5) #
		plt.axis('off')
		plt.savefig(images_for_plots+'/'+patient_name+'_MRI_SAX_circ_2lines.png',bbox_inches='tight',dpi=800)
		plt.close()	

	data = np.loadtxt(strain_folder+'/Radial_strains.txt')
	with open(images_for_plots+'/'+patient_name+'_radial.txt','w') as ff:
		for i in range(data.shape[0]):
			ff.write('%d %1.4f %1.4f %1.4f\n' %(i,data[i,1],data[i,1]-data[i,2],data[i,1]+data[i,2]))

	if os.path.isfile(strain_folder+'/Radial_strains_scar.txt'):
		data = np.loadtxt(strain_folder+'/Radial_strains_scar.txt')
		with open(images_for_plots+'/'+patient_name+'_radial_scar.txt','w') as ff:
			for i in range(data.shape[0]):
				ff.write('%d %1.4f %1.4f %1.4f\n' %(i,data[i,1],data[i,1]-data[i,2],data[i,1]+data[i,2]))

	data = np.loadtxt(strain_folder+'/Circumferentual_strains.txt')
	with open(images_for_plots+'/'+patient_name+'_circumferential.txt','w') as ff:
		for i in range(data.shape[0]):
			ff.write('%d %1.4f %1.4f %1.4f\n' %(i,data[i,1],data[i,1]-data[i,2],data[i,1]+data[i,2]))

	if os.path.isfile(strain_folder+'/Circumferentual_strains_scar.txt'):
		data = np.loadtxt(strain_folder+'/Circumferentual_strains_scar.txt')
		with open(images_for_plots+'/'+patient_name+'_circumferential_scar.txt','w') as ff:
			for i in range(data.shape[0]):
				ff.write('%d %1.4f %1.4f %1.4f\n' %(i,data[i,1],data[i,1]-data[i,2],data[i,1]+data[i,2]))

	data = np.loadtxt(strain_folder+'/Longitudinal_strains.txt')
	with open(images_for_plots+'/'+patient_name+'_longitudinal.txt','w') as ff:
		for i in range(data.shape[0]):
			ff.write('%d %1.4f %1.4f %1.4f\n' %(i,data[i,1],data[i,1]-data[i,2],data[i,1]+data[i,2]))

	if os.path.isfile(strain_folder+'/Longitudinal_strains_scar.txt'):
		data = np.loadtxt(strain_folder+'/Longitudinal_strains_scar.txt')
		with open(images_for_plots+'/'+patient_name+'_longitudinal_scar.txt','w') as ff:
			for i in range(data.shape[0]):
				ff.write('%d %1.4f %1.4f %1.4f\n' %(i,data[i,1],data[i,1]-data[i,2],data[i,1]+data[i,2]))