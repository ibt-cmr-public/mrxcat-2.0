def getBoundaryPoints(mask,nodes = None):

	import numpy as np
	from matplotlib import pyplot as plt	
	from scipy.ndimage.morphology import binary_erosion, binary_fill_holes
	import cv2
	# from skimage.measure import find_contours
	from skimage.morphology import convex_hull_image
	'''
	takes a binary mask (assuming that the white bit is just one connected component)
	and returns the list of points going clockwise around the edge of the region
	'''

	# dims_mask = mask.shape
	mask = 1*convex_hull_image(mask)
	# assert len(mask.shape) == 2
	contours, hier = cv2.findContours(mask,2,1)
	#contours = find_contours(mask)[0]	
	border = np.array(contours[0]).reshape(-1,2)
	if nodes is not None:
		step_size = np.max([1,np.floor(border.shape[0]/(target_num_pts))])
		pts       = border[0:border.shape[0]:int(step_size),:]
	else:
		pts = border[0:border.shape[0]:1]
	# if target_num_pts == border.shape[0]:
	# 	print('All points are selected')
	# 	pts = border
	# else:

	# if target_num_pts == border.shape[0]:
	# 	print('All points are selected')
	# 	pts = border
	# else:
	#pts = border[0:border.shape[0]:int(step_size),:]
	# if pts.shape[0]<target_num_pts and step_size>1:
	# 	pts = border[0:border.shape[0]:int(step_size)-1,:]		
	# 	pts = pts[0:target_num_pts,:]

	return pts

def adjustNumberPoints(pts10,pts20):

	import numpy as np

	if pts10.shape[0] < pts20.shape[0]:
		pts1 = np.zeros((pts10.shape[0],2))
		pts2 = np.zeros((pts10.shape[0],2))
		ratio_points = pts20.shape[0]/pts10.shape[0]-1
		skip = 0
		residual = 0
		for ii in range(pts10.shape[0]):
			pts1[ii,:] = pts10[ii,:]
			pts2[ii,:] = pts20[skip+ii,:]
			residual += ratio_points
			if residual >= 1:
				skip += 1
				residual -= 1
	elif pts10.shape[0] > pts20.shape[0]:
		pts1 = np.zeros((pts20.shape[0],2))
		pts2 = np.zeros((pts20.shape[0],2))
		ratio_points = pts10.shape[0]/pts20.shape[0]-1
		skip = 0
		residual = 0
		for ii in range(pts20.shape[0]):
			pts2[ii,:] = pts20[ii,:]
			pts1[ii,:] = pts10[skip+ii,:]
			residual += ratio_points
			if residual >= 1:
				skip += 1
				residual -= 1
	else:
		pts1 = pts10
		pts2 = pts20

	# if num_points is not None:
	# 	step_size = np.max([1,np.floor(border.shape[0]/(target_num_pts))])
	# 	border = border[0:border.shape[0]:int(step_size),:]


	return pts1,pts2

def alignPoints3D_backup(pts1,pts2):
	import numpy as np
	from scipy.spatial import distance

	'''
	given two ordered lists of points of the same length we want to minimise the distance between the pairt of points
	we do this by finding the best first point in the first list
	'''

	assert len(pts1) == len(pts2)
	# Fix first point of pts2, find closest one in pts1
	first_index = distance.cdist(pts1,[pts2[0,:]]).argmin()
	pts1 = np.concatenate((pts1[first_index:,:],pts1[:first_index,:]),0)
	return pts1

def alignPoints(pts1,pts2,cx1,cy1,cx2,cy2):
	import numpy as np
	# from scipy.ndimage.measurements import center_of_mass as com
	'''
	given two ordered lists of points of the same length we want to minimise the distance between the pairt of points
	we do this by mapping the points in polar coordinates and pairing them
	'''
	#assert len(pts1) == len(pts2)

	# # Compute center of mass and polar coordinates for each point
	# cx1,cy1 = np.mean(pts1,0)
	# cx2,cy2 = np.mean(pts2,0)

	angles1 = np.zeros((pts1.shape[0],1))

	for ii in range(pts1.shape[0]):
		relx = float(pts1[ii,0] - cx1); rely = float(pts1[ii,1] - cy1)
		angles1[ii] = np.arctan2(rely,relx)

	# Check sorting

	start_index = abs(angles1).argmin()
	pts1 = np.concatenate((pts1[start_index:,:],pts1[:start_index,:]),0)
	# if angles1[start_index] < angles1[start_index+1]:
	# 	print('Reversing ordering direction pts1')
	# 	pts1 = pts1[0:pts1.shape[0]:-1,:]

	angles2 = np.zeros((pts2.shape[0],1))
	for ii in range(pts2.shape[0]):
		relx = float(pts2[ii,0] - cx2); rely = float(pts2[ii,1]- cy2)
		angles2[ii] = np.arctan2(rely,relx)
	
	start_index = np.argmin(abs(angles2))

	pts2    = np.concatenate((pts2[start_index:,:],pts2[:start_index,:]),0)
	# if angles2[start_index] < angles2[start_index+1]:
	# 	print('Reversing ordering direction pts2')
	# 	pts2 = pts2[0:pts2.shape[0]:-1,:]
	# angles2 = np.concatenate((angles2[angle2_start:,:],angles2[:angle2_start,:]),0)

	# angle2_ref = angles2[0]


	return pts1,pts2

def alignPointsAndResample(pts1,pts2,cx1,cy1,cx2,cy2,n_points = 30):
	import numpy as np
	from scipy.interpolate import interp1d

	theta1  = np.zeros((pts1.shape[0],1))
	radius1 = np.zeros((pts1.shape[0],1))
	for ii in range(pts1.shape[0]):
		relx = float(pts1[ii,0] - cx1); rely = float(pts1[ii,1] - cy1)
		theta1[ii]  = np.arctan2(rely,relx)
		radius1[ii] = np.sqrt(relx**2+rely**2)

	theta_periodic = np.concatenate((theta1-2*np.pi,theta1,theta1+2*np.pi),0)
	r_periodic = np.concatenate((radius1,radius1,radius1),0)
	# Theta is between -pi and pi
	# Resample it at equally spaced intervals with interpolation
	f = interp1d(theta_periodic[:,0], r_periodic[:,0])
	theta1_resampled  = np.linspace(-np.pi,np.pi,n_points)
	radius1_resampled = f(theta1_resampled)
	pts1_resampled = np.zeros((n_points,2))
	for ii in range(n_points):
		pts1_resampled[ii,:] = [radius1_resampled[ii]*np.cos(theta1_resampled[ii]) + cx1,radius1_resampled[ii]*np.sin(theta1_resampled[ii]) + cy1]

	theta2 = np.zeros((pts2.shape[0],1))
	radius2 = np.zeros((pts2.shape[0],1))
	for ii in range(pts2.shape[0]):
		relx = float(pts2[ii,0] - cx2); rely = float(pts2[ii,1]- cy2)
		theta2[ii] = np.arctan2(rely,relx)
		radius2[ii] = np.sqrt(relx**2+rely**2)	

	theta_periodic = np.concatenate((theta2-2*np.pi,theta2,theta2+2*np.pi),0)
	r_periodic = np.concatenate((radius2,radius2,radius2),0)
	f = interp1d(theta_periodic[:,0], r_periodic[:,0])
	theta2_resampled  = np.linspace(-np.pi,np.pi,n_points)
	radius2_resampled = f(theta2_resampled)
	pts2_resampled = np.zeros((n_points,2))
	for ii in range(n_points):
		pts2_resampled[ii,:] = [radius2_resampled[ii]*np.cos(theta2_resampled[ii]) + cx2,radius2_resampled[ii]*np.sin(theta2_resampled[ii]) + cy2]

	return pts1_resampled,pts2_resampled

def warpImages3D(Target_mask,BP_fill,data_input,maskLabels,frameRef):

	import numpy as np
	import cv2
	from scipy.interpolate import CloughTocher2DInterpolator
	from skimage.morphology import dilation
	from scipy.ndimage import gaussian_filter


	dims       = data_input.shape
	XCAT_LV    = np.zeros(dims,dtype='int')
	XCAT_LV[np.where(data_input == maskLabels.LV_wall)] = 1

	XCAT_RV    = np.zeros(dims,dtype='int')
	XCAT_RV[np.where(data_input == maskLabels.RV_blood)] = 1

	XCAT_LVb   = np.zeros(dims,dtype='int')
	XCAT_LVb[np.where(data_input == maskLabels.LV_blood)] = 1

	xLVxcat,yLVxcat,zLVxcat = np.where(XCAT_LV == 1)
	xRVxcat,yRVxcat,zRVxcat = np.where(XCAT_RV == 1)

	min_box_x = xLVxcat.min(); max_box_x = xLVxcat.max()
	min_box_y = yLVxcat.min(); max_box_y = yLVxcat.max()
	XCATcenterBox_x = int(0.5*(min_box_x+max_box_x))
	XCATcenterBox_y = int(0.5*(min_box_y+max_box_y))

	XCAT_exterior    = np.zeros(dims,dtype='int')
	XCAT_exterior[np.where(data_input == 0)] = 1

	# Box extension
	box_x = (max_box_x - min_box_x)
	box_y = (max_box_y - min_box_y)
	box_x = np.max([box_x,box_y])

	# Estimate extension of the LV, and estimates extension up to RV
	min_box_xwRV = np.min([xLVxcat.min(),xRVxcat.min()]); max_box_xwRV = np.max([xLVxcat.max(),xRVxcat.max()])
	min_box_ywRV = np.min([yLVxcat.min(),yRVxcat.min()]); max_box_ywRV = np.max([yLVxcat.max(),yRVxcat.max()])
	# Box extension
	box_xwRV = (max_box_xwRV - min_box_xwRV)
	box_ywRV = (max_box_ywRV - min_box_ywRV)
	box_xwRV = np.max([box_xwRV,box_ywRV])*0.5

	# Interpolation on the full image
	XX, YY = np.meshgrid(np.arange(dims[0]),np.arange(dims[1]),indexing='ij')
	CC     = np.concatenate((XX.reshape(-1,1,order='F'),YY.reshape(-1,1,order='F')),1)

	# Dimensions for box at boundaries of image
	min_box_x2 = 0; max_box_x2 = dims[0]; 
	min_box_y2 = 0; max_box_y2 = dims[1]; 

	box_x2 = (max_box_x2 - min_box_x2)
	box_y2 = (max_box_y2 - min_box_y2)

	map_x = np.zeros(dims)
	map_y = np.zeros(dims)

	min_ii = -1
	max_ii = 0

	mask_4_interp_ind = np.where(np.power(XX-XCATcenterBox_x,2)+np.power(YY-XCATcenterBox_y,2)-box_xwRV**2<0)
	mask_4_interp = np.zeros_like(XX)
	mask_4_interp[mask_4_interp_ind] = 1.0

	n_points_circle = 15 # hard-coded, seems to be enough points here
	n_points_in_edge = n_points_circle//4 # hard-coded, seems to be enough points here
	theta_ = np.linspace(0,np.pi*2*(n_points_circle-1)/(n_points_circle),n_points_circle)

	for ii in range(Target_mask.shape[2]):

		if np.sum(Target_mask[:,:,ii])>20 and np.sum(XCAT_LV[:,:,ii])>20:
			if min_ii == -1:
				min_ii = ii
			max_ii = ii

			# Get LV point at epicardium
			mask1  = Target_mask[:,:,ii]
			mask2  = XCAT_LV[:,:,ii]

			pts1      = getBoundaryPoints(mask1) # MSK 1 is the target one, from the mesh
			pts2      = getBoundaryPoints(mask2)
			pts1,pts2 = alignPointsAndResample(pts1,pts2, XCATcenterBox_y, XCATcenterBox_x, XCATcenterBox_y, XCATcenterBox_x,n_points_circle) #pair the points using polar coordinates relative to cdm

			ptsOUT    = getBoundaryPoints(XCAT_exterior[...,ii])

			pts2 = np.concatenate((pts2,ptsOUT[::2,:]),0)
			pts1 = np.concatenate((pts1,ptsOUT[::2,:]),0)

			maskRV        = XCAT_RV[...,ii]
			ptsRVx,ptsRVy = np.where(maskRV==1)
			ptsRV         = np.concatenate((ptsRVx[::2].reshape(-1,1),ptsRVy[::2].reshape(-1,1)),1)

			pts2 = np.concatenate((pts2,ptsRV),0)
			pts1 = np.concatenate((pts1,ptsRV),0)
			
			extra_pts2 = np.zeros((n_points_circle,2))
			extra_pts1 = np.zeros((n_points_circle,2))

			#displT    = pts1 - pts2
			for kl in range(n_points_circle):
				extra_pts2[kl,:] = [XCATcenterBox_x + np.cos(theta_[kl])*box_xwRV ,XCATcenterBox_y + np.sin(theta_[kl])*box_xwRV]
				extra_pts1[kl,0] = extra_pts2[kl,0]
				extra_pts1[kl,1] = extra_pts2[kl,1]

			extra_pts2 = np.flip(extra_pts2,1)
			pts2       = np.concatenate((pts2,extra_pts2),0)
			extra_pts1 = np.flip(extra_pts1,1)
			pts1       = np.concatenate((pts1,extra_pts1),0)

			# extra_ptsBD = np.zeros((n_points_in_edge,2))
			# for kl in range(n_points_in_edge):
			# 	extra_ptsBD[kl,:] = [dims[0]//2 + np.cos(theta_[kl])*dims[0] ,dims[0]//2 + np.sin(theta_[kl])*dims[0]]
			# extra_ptsBD = np.flip(extra_ptsBD,1)
			extra_ptsBD = np.zeros((n_points_in_edge*4,2))
			for kl in range(n_points_in_edge):
				extra_ptsBD[kl,:]                    = [min_box_x2,min_box_y2+int(box_y2*kl/(n_points_in_edge-1))]
				extra_ptsBD[kl+n_points_in_edge,:]   = [max_box_x2,min_box_y2+int(box_y2*kl/(n_points_in_edge-1))]
				extra_ptsBD[kl+2*n_points_in_edge,:] = [min_box_x2+int(box_x2*kl/(n_points_in_edge-1)),min_box_y2]
				extra_ptsBD[kl+3*n_points_in_edge,:] = [min_box_x2+int(box_x2*kl/(n_points_in_edge-1)),max_box_y2]
			extra_ptsBD = np.flip(extra_ptsBD,1)

			pts1 = np.concatenate((pts1,extra_ptsBD),0)
			pts2 = np.concatenate((pts2,extra_ptsBD),0)

			if np.sum(BP_fill[:,:,ii])>5 and np.sum(XCAT_LVb[:,:,ii])>5:
				# Get LV point at epicardium
				mask1b  = BP_fill[:,:,ii]
				mask2b  = XCAT_LVb[:,:,ii]

				pts1b = getBoundaryPoints(mask1b)
				pts2b = getBoundaryPoints(mask2b)
				pts1b,pts2b = alignPointsAndResample(pts1b,pts2b,XCATcenterBox_y,XCATcenterBox_x,XCATcenterBox_y,XCATcenterBox_x,n_points_circle) #pair the points using polar coordinates relative to cdm

				pts1 = np.concatenate((pts1,pts1b),0)
				pts2 = np.concatenate((pts2,pts2b),0)

			interpx   = CloughTocher2DInterpolator(np.flip(pts1,1), pts2[:,0],fill_value=0)#,tol=1e-06, maxiter=600)
			interpy   = CloughTocher2DInterpolator(np.flip(pts1,1), pts2[:,1],fill_value=0)#,tol=1e-06, maxiter=600)
			maskMaxLV = dilation(dilation(dilation(dilation(np.ma.mask_or(np.array(mask1,dtype='int32'), np.array(mask2,dtype='int32')) ))))

			map_x[:,:,ii] = np.multiply(mask_4_interp,interpx(CC).reshape(dims[0],dims[1],order='F')) + np.multiply(1-mask_4_interp,YY) 
			map_y[:,:,ii] = np.multiply(mask_4_interp,interpy(CC).reshape(dims[0],dims[1],order='F')) + np.multiply(1-mask_4_interp,XX)
			map_x[...,ii] = gaussian_filter(map_x[:,:,ii], sigma = 1.0)
			map_y[...,ii] = gaussian_filter(map_y[:,:,ii], sigma = 1.0)

			data_input[:,:,ii]  = cv2.remap(np.array(data_input[:,:,ii],dtype='float32'),np.array(map_x[:,:,ii],dtype='float32'),np.array(map_y[:,:,ii],dtype='float32'),cv2.INTER_NEAREST)

		else:
			map_x[:,:,ii] = YY
			map_y[:,:,ii] = XX

	if frameRef:
		for ii in range(Target_mask.shape[2]):
			if ii < min_ii:
				map_x[:,:,ii] = map_x[:,:,min_ii]
				map_y[:,:,ii] = map_y[:,:,min_ii]

			elif ii > max_ii:
				map_x[:,:,ii] = map_x[:,:,max_ii]
				map_y[:,:,ii] = map_y[:,:,max_ii]

	return data_input, map_x, map_y , XCATcenterBox_x, XCATcenterBox_y

def computeScalingImage(mesh_path,img_path,maskLabels):

	import vtk
	from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
	import numpy as np

	# Read initial, reference mesh
	displ_reader  = vtk.vtkUnstructuredGridReader()
	displ_reader.ReadAllScalarsOn()
	displ_reader.ReadAllVectorsOn()
	displ_reader.SetFileName(mesh_path)
	displ_reader.Update()   
	mesh = displ_reader.GetOutput() 
	Coords_LV = vtk_to_numpy(mesh.GetPoints().GetData())

	min_x_FEM =Coords_LV[:,0].min();max_x_FEM = Coords_LV[:,0].max()
	min_y_FEM =Coords_LV[:,1].min();max_y_FEM = Coords_LV[:,1].max()
	min_z_FEM =Coords_LV[:,2].min();max_z_FEM = Coords_LV[:,2].max()

	LV_FEM_x = abs(max_x_FEM - min_x_FEM)
	LV_FEM_y = abs(max_y_FEM - min_y_FEM)
	LV_FEM_z = abs(max_z_FEM - min_z_FEM)

	read_img = vtk.vtkXMLImageDataReader()
	read_img.SetFileName(img_path)
	read_img.Update()
	img_ref    = read_img.GetOutput()
	dims       = img_ref.GetDimensions()
	orig_image = img_ref.GetOrigin()
	data_input = vtk_to_numpy(img_ref.GetPointData().GetArray('labels')).reshape(dims[0],dims[1],dims[2],order="F")
	spacing    = img_ref.GetSpacing()

	# Get LV masks from MRXCAT image
	XCAT_LV_mask = np.zeros(data_input.shape)
	sel_points   = np.where(data_input == maskLabels.LV_wall)
	XCAT_LV_mask[sel_points] = 1

	x_XCAT, y_XCAT, z_XCAT= np.where(XCAT_LV_mask==1)
	min_x_XCAT = x_XCAT.min();max_x_XCAT = x_XCAT.max()
	min_y_XCAT = y_XCAT.min();max_y_XCAT = y_XCAT.max()
	min_z_XCAT = z_XCAT.min();max_z_XCAT = z_XCAT.max()

	LV_XCAT_x = abs(max_x_XCAT - min_x_XCAT)*spacing[0]
	LV_XCAT_y = abs(max_y_XCAT - min_y_XCAT)*spacing[1]
	LV_XCAT_z = abs(max_z_XCAT - min_z_XCAT)*spacing[2]

	scaling_x = np.min([LV_FEM_x/LV_XCAT_x,LV_FEM_y/LV_XCAT_y])
	scaling_y = scaling_x
	scaling_z = LV_FEM_z/LV_XCAT_z

	print('Scaling image with ratios [%1.4f, %1.4f, %1.4f]' %(scaling_x,scaling_y,scaling_z))

	spacing = [spacing[0]*scaling_z,spacing[1]*scaling_z,spacing[2]*scaling_z]
	# Get LV masks from MRXCAT image
	max_z_XCAT = z_XCAT.max()*spacing[2]+orig_image[2]
	MV_centerx,MV_centery = np.where(data_input[:,:,z_XCAT.max()-1] == maskLabels.LV_wall)
	orig_image = [-np.mean(MV_centerx)*spacing[0],-np.mean(MV_centery)*spacing[1],-(z_XCAT.max()-1)*spacing[2]]

	return spacing,orig_image

def scaleImage(img_ref,spacing,orig_image):

	import vtk

	img_ref_new = vtk.vtkImageChangeInformation()
	img_ref_new.SetInputData(img_ref)
	img_ref_new.SetOutputSpacing(spacing)
	img_ref_new.SetOutputOrigin(orig_image[0],orig_image[1],orig_image[2])
	img_ref_new.Update()
	img_ref  = img_ref_new.GetOutput()

	return img_ref

def vtkSliceImage(imgArray,spacing,orig_image):

	import vtk

	# Generate VTK image
	ref_image = vtk.vtkImageData()
	ref_image.SetDimensions(imgArray.shape[0], imgArray.shape[1],1)
	ref_image.SetSpacing(spacing)
	ref_image.SetOrigin(orig_image[0], orig_image[1], orig_image[2])

	return ref_image

def addArrayToVtk(vtk_obj,data_input,label,is3D=False):
	import numpy as np
	from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk

	if is3D:
		linear_array = np.array(data_input)
		array_vtk = numpy_to_vtk(linear_array)
		array_vtk.SetNumberOfComponents(3)
	else:
		linear_array = np.array(data_input).reshape(-1, 1, order="F")
		array_vtk = numpy_to_vtk(linear_array)
	array_vtk.SetName(label)
	vtk_obj.GetPointData().AddArray(array_vtk)
	vtk_obj.Modified()

	return vtk_obj

def saveVTI(img,out_path):
	import vtk

	writer = vtk.vtkXMLImageDataWriter()
	writer.SetFileName(out_path)
	writer.SetInputData(img)
	writer.Write()

	return 0

def warpSlice(Target_mask,data_input,SAX_slice_selection,maskLabels,frameRef_val):
	import numpy as np
	from scipy.ndimage import binary_fill_holes

	Target_mask = np.expand_dims(Target_mask[:,:,SAX_slice_selection],-1)
	if data_input.shape[2] > 1:
		data_input  = np.expand_dims(data_input[:,:,SAX_slice_selection],-1)

	# Compute LV blood pool mask from Target mask
	BP_fill = np.expand_dims(np.multiply(1-Target_mask[...,0],binary_fill_holes(Target_mask[...,0]).astype(int)),-1)
	data_input, mapx0, mapy0, centerBox_x, centerBox_y = warpImages3D(Target_mask, BP_fill, data_input, maskLabels,
																	  frameRef=frameRef_val)
	data_input         = np.rint(data_input)
	BP_fill = np.expand_dims(np.multiply(1-Target_mask[...,0],binary_fill_holes(Target_mask[...,0]).astype(int)),-1)
	data_input[np.where(BP_fill == 1)]   = maskLabels.LV_blood
	data_input         = data_input * (1 - Target_mask) + Target_mask

	return data_input, Target_mask, BP_fill, mapx0,mapy0,centerBox_x,centerBox_y

def getParValues(s):
	newstr = ''.join((ch if ch in '0123456789.' else ' ') for ch in s)
	listOfN = [float(i) for i in newstr.split()]
	return listOfN


def run_MRXCAT(param_file,target_heart_phases,par_file_folder,output_case):

	import os, shutil
	import numpy as np
	import vtk
	from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk

	base_name           = 'Image'

	param_filev_beating = param_file.rstrip('.par')+'_beating_vectors.par'
	base_name_beating   = base_name +'_beating'

	param_filev_breathing = param_file.rstrip('.par')+'_breathing_vectors.par'
	base_name_breathing   = base_name +'_breathing'

	output_XCAT        = output_case + '/XCAT_output/'
	output_VTI_phantom = output_case + '/XCAT_vti/'

	if not os.path.exists(output_case):
		os.makedirs(output_case)
	if not os.path.exists(output_XCAT):
		os.makedirs(output_XCAT)
	if not os.path.exists(output_VTI_phantom):
		os.makedirs(output_VTI_phantom)

	# Generate new XCAT model with FULL motion (beating + breathing)
	# Angles for the view which are approx well SAX acquisitions
	xrot = 130.0
	yrot = 35.0
	zrot = 240.0

	with open('%s' % (par_file_folder+'/'+param_file,),'r') as infile:
		data = infile.readlines()

	data[0] = 'mode = 0\n'
	data[6] = 'motion_option = 2\n'

	# Specifies target frames for XCAT output
	data[9]  = 'time_per_frame = %1.2f\n' %(1.0/target_heart_phases)
	data[10] = 'out_frames     = %d\n' %(target_heart_phases)

	with open('%s' % (output_case+'/'+param_file,),'w') as outfile:
		outfile.writelines(data)

		os.chdir("XCAT_core")
		os.system('dxcat2.exe %s/%s --phan_rotx %.1f --phan_roty %.1f --phan_rotz %.1f %s/%s' % (output_case,param_file, xrot, yrot, zrot, output_XCAT,base_name))
		os.chdir("..")
		shutil.copyfile(output_case+'/'+param_file,output_XCAT+'/'+param_file)

	# Modify par file so that the same parameters are used in the vector simulation
	# First ONLY BEATING MOTION is selected
	with open('%s' % (par_file_folder+'/'+param_file,),'r') as infile:
		data = infile.readlines()

	data[0] = 'mode = 4\n'
	data[6] = 'motion_option = 0\n'
	data[9] = 'time_per_frame = %1.2f\n' %(1.0/target_heart_phases)
	data[10] = 'out_frames     = %d\n' %(target_heart_phases)

	with open('%s' % (output_case+'/'+param_filev_beating,),'w') as outfile:
		outfile.writelines(data)

	# Generates vectors
	os.chdir("XCAT_core")
	os.system('dxcat2.exe %s/%s --phan_rotx %.1f --phan_roty %.1f --phan_rotz %.1f %s/%s' % (output_case,param_filev_beating, xrot, yrot, zrot, output_XCAT,base_name_beating))
	os.chdir("..")

	shutil.copyfile(output_case+'/'+param_filev_beating,output_XCAT+'/'+param_filev_beating)

	with open('%s' % (par_file_folder+'/'+param_file,),'r') as infile:
		data = infile.readlines()

	data[0] = 'mode = 4\n'
	data[6] = 'motion_option = 1\n'
	data[9] = 'time_per_frame = %1.2f\n' %(1.0/target_heart_phases)
	data[10] = 'out_frames     = %d\n' %(target_heart_phases)

	with open('%s' % (output_case+'/'+param_filev_breathing,),'w') as outfile:
		outfile.writelines(data)

	# Generates vectors
	os.chdir("XCAT_core")
	os.system('dxcat2.exe %s/%s --phan_rotx %.1f --phan_roty %.1f --phan_rotz %.1f %s/%s' % (output_case,param_filev_breathing, xrot, yrot, zrot, output_XCAT,base_name_breathing))
	os.chdir("..")

	shutil.copyfile(output_case+'/'+param_filev_breathing,output_XCAT+'/'+param_filev_breathing)

	# Store in VTI data the phantom tissue and the displacement map
	# Get .bin files (phantoms over time)
	frames_0 = np.sort(next(os.walk(output_XCAT))[2])
	frames_img = []
	for frame_sel in frames_0:
		if frame_sel[-3:] == 'bin':
			frames_img.append(frame_sel)

	# Get displacement fields over time
	frames_vect_breating = []
	frames_vect_beating  = []
	for frame_sel in frames_0:
		if frame_sel[-3:] == 'txt':
			if 'breating' in frame_sel:
				frames_vect_breating.append(frame_sel)
			elif 'beating' in frame_sel:
				frames_vect_beating.append(frame_sel)

	# Get resolution from _log file
	log_img_file = frames_img[0].rstrip('act_1.bin')
	with open(output_XCAT+'/%s_log' % (log_img_file,),'r') as infile:
		data = infile.readlines()

	for l_sel in data:
		if 'pixel width =' in l_sel:
			res_plane = getParValues(l_sel)[0]*0.01
		elif 'slice width =' in l_sel:
			res_slice = getParValues(l_sel)[0]*0.01
		elif 'array_size =' in l_sel:
			arr_size = int(getParValues(l_sel)[0])
		elif 'starting slice number ' in l_sel:
			sl_start                  = int(getParValues(l_sel)[0])
		elif 'ending slice number ' in l_sel:
			sl_end                    = int(getParValues(l_sel)[0])

	dims_xcat       = (sl_end-sl_start+1,arr_size,arr_size)
	dims_np         = (arr_size,arr_size,sl_end-sl_start+1)
	res_vector_np   = (res_plane,res_plane,res_slice)

	# Convert .bin images to vti
	counterf = 1
	for f_sel in (frames_img):
		print('Processing frame %d from folder %s' %(counterf,output_XCAT))

		x = np.fromfile('%s' % (output_XCAT+'/'+f_sel),'float32')
		image_xc = np.real(np.reshape(x,dims_xcat))
		print('... XCAT output dimensions are ',dims_xcat)
		# image = np.rot90(image, 1, (0,2))
		# image = np.flip(image, 2)
		out_img_shape = dims_np
		print('... Image output dimensions are ',dims_np)
		# # Reshape image so short axis normal is along 3rd dimension
		image = np.zeros(dims_np)
		for i in range(dims_xcat[0]):
			image[:,:,dims_xcat[0]-i-1] = image_xc[i,:,:]

		displx_beating = np.zeros_like(image)
		disply_beating = np.zeros_like(image)
		displz_beating = np.zeros_like(image)

		displx_breathing = np.zeros_like(image)
		disply_breathing = np.zeros_like(image)
		displz_breathing = np.zeros_like(image)

		# Compute origin of image so that Mitral Valve center is at [0,0,0]
		if counterf == 1:
			for i in range(image.shape[2]):
				has_LV = np.where(image[:,:,i] == 1)[0]
				if has_LV.size > 0:
					z_c = i

			x_c,y_c   = np.where(image[:,:,z_c] == 1)
			orig_x    = -np.mean(x_c)*res_plane
			orig_y    = -np.mean(y_c)*res_plane
			orig_z    = -z_c*res_slice
			orig_img  = (orig_x,orig_y,orig_z)

			# Set image dimension and position
			vti_image_xcat = vtk.vtkImageData()
			vti_image_xcat.SetDimensions(out_img_shape)
			vti_image_xcat.SetSpacing(res_vector_np)
			vti_image_xcat.SetOrigin(orig_img)

		else:
			file_txt = '%s_vec_frame1_to_frame%d.txt' %(base_name_beating,counterf)

			if os.path.isfile(output_XCAT+'/%s' % (file_txt,)):
				with open(output_XCAT+'/%s' % (file_txt,),'r') as infile:
					data_beating = infile.readlines()

				map_field_beating = np.zeros((len(data_beating)-2,6))
				for ii in range(2,len(data_beating)):
					splitS = data_beating[ii].split()
					map_field_beating[ii-2,:] = [int(splitS[2]),int(splitS[3]),int(splitS[4]),float(splitS[6])-int(splitS[2]),float(splitS[7])-int(splitS[3]),float(splitS[8])-int(splitS[4])]

				map_field_beating[:,2] = out_img_shape[2]-1-map_field_beating[:,2]
				displx_beating[list(map(int, map_field_beating[:,1])),list(map(int, map_field_beating[:,0])),list(map(int, map_field_beating[:,2]))] = map_field_beating[:,4]
				disply_beating[list(map(int, map_field_beating[:,1])),list(map(int, map_field_beating[:,0])),list(map(int, map_field_beating[:,2]))] = map_field_beating[:,3]
				displz_beating[list(map(int, map_field_beating[:,1])),list(map(int, map_field_beating[:,0])),list(map(int, map_field_beating[:,2]))] = - map_field_beating[:,5]
			else:
				print('... ... Beathing file vector not found')
			file_txt = '%s_vec_frame1_to_frame%d.txt' %(base_name_breathing,counterf)
			if os.path.isfile(output_XCAT+'/%s' % (file_txt,)):
				with open(output_XCAT+'/%s' % (file_txt,),'r') as infile:
					data_breathing = infile.readlines()

				map_field_breathing = np.zeros((len(data_breathing)-2,6))
				for ii in range(2,len(data_breathing)):
					splitS = data_breathing[ii].split()
					map_field_breathing[ii-2,:] = [int(splitS[2]),int(splitS[3]),int(splitS[4]),float(splitS[6])-int(splitS[2]),float(splitS[7])-int(splitS[3]),float(splitS[8])-int(splitS[4])]

				map_field_breathing[:,2] = out_img_shape[2]-1-map_field_breathing[:,2]
				displx_breathing[list(map(int, map_field_breathing[:,1])),list(map(int, map_field_breathing[:,0])),list(map(int, map_field_breathing[:,2]))] = map_field_breathing[:,4]
				disply_breathing[list(map(int, map_field_breathing[:,1])),list(map(int, map_field_breathing[:,0])),list(map(int, map_field_breathing[:,2]))] = map_field_breathing[:,3]
				displz_breathing[list(map(int, map_field_breathing[:,1])),list(map(int, map_field_breathing[:,0])),list(map(int, map_field_breathing[:,2]))] = - map_field_breathing[:,5]
			else:
				print('... ... Breating file vector not found')

		# Add mask labels and store vti
		linear_array = image.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('labels')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()

		linear_array = displx_beating.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('dx_beating')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()
		linear_array = disply_beating.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('dy_beating')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()
		linear_array = displz_beating.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('dz_beating')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()

		linear_array = displx_breathing.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('dx_breathing')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()
		linear_array = disply_breathing.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('dy_breathing')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()
		linear_array = displz_breathing.reshape(-1,1,order="F")
		vtk_array = numpy_to_vtk(linear_array)
		vtk_array.SetName('dz_breathing')
		vti_image_xcat.GetPointData().AddArray(vtk_array)
		vti_image_xcat.Modified()

		writer = vtk.vtkXMLImageDataWriter()
		writer.SetFileName(output_VTI_phantom+'/Image_%02d.vti' %(counterf))
		writer.SetInputData(vti_image_xcat)
		writer.Write()

		counterf +=1


def vti2Bin(input_image_folder, results_folder_SAX, targetImg_size_SAX):

	import numpy as np
	import os
	import vtk
	from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
	import matplotlib.pyplot as plt

	read_img = vtk.vtkXMLImageDataReader()

	texture_properties = ['PD', 'T1', 'T2', 'T2s']

	if not os.path.exists(results_folder_SAX):
		os.makedirs(results_folder_SAX)

	for fol_sel in texture_properties:
		for dir_main in [results_folder_SAX]:
			if not os.path.exists(dir_main + '/' + fol_sel):
				os.makedirs(dir_main + '/' + fol_sel)

	frames_0 = np.sort(next(os.walk(input_image_folder + '/XCAT_warped_vti/'))[2])
	frames = []
	for frame_sel in frames_0:
		if frame_sel[0] != '.':
			if frame_sel[-3:] == 'vti':
				frames.append(frame_sel)

	read_img.SetFileName(input_image_folder + '/XCAT_warped_vti/' + frames[0])
	read_img.Update()
	spacing = read_img.GetOutput().GetSpacing()
	dims = read_img.GetOutput().GetDimensions()
	orig_image = read_img.GetOutput().GetOrigin()

	for counter, frame_sel in enumerate(frames):

		read_img.SetFileName(input_image_folder + '/XCAT_warped_vti/' + frame_sel)
		read_img.Update()
		data_input = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('labels')).reshape(dims[0], dims[1],
																								  dims[2], order="F")
		PD = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('PD')).reshape(dims[0], dims[1], dims[2],
																					  order="F")
		T1 = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('T1')).reshape(dims[0], dims[1], dims[2],
																					  order="F")
		T2 = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('T2')).reshape(dims[0], dims[1], dims[2],
																					  order="F")
		T2s = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('T2s')).reshape(dims[0], dims[1], dims[2],
																						order="F")
		e_radial = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('e_radial')).reshape(dims[0], dims[1],
																								  dims[2], order="F")
		e_circ = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('e_circ')).reshape(dims[0], dims[1], dims[2],
																							  order="F")
		e_long = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('e_long')).reshape(dims[0], dims[1], dims[2],
																							  order="F")
		LV_mask = vtk_to_numpy(read_img.GetOutput().GetPointData().GetArray('LV_mask')).reshape(dims[0], dims[1],
																								dims[2], order="F")

		if counter == 0:
			# Define cropping of the image
			idx = np.where(LV_mask == 1)
			xLV, yLV, _ = idx[0], idx[1], idx[2]
			max_x, min_x = xLV.max(), xLV.min()
			max_y, min_y = yLV.max(), yLV.min()

			xCenter_SAX = int(0.5 * (max_x + min_x))
			yCenter_SAX = int(0.5 * (max_y + min_y))
			extension_SAX = np.max([max_x - xCenter_SAX, max_y - yCenter_SAX])

		cropImage_SAX = np.array(data_input[np.max([int(xCenter_SAX - targetImg_size_SAX[0] // 2), 0]):np.min(
			[int(xCenter_SAX + targetImg_size_SAX[0] // 2), dims[0]]),
								 np.max([int(yCenter_SAX - targetImg_size_SAX[1] // 2), 0]):np.min(
									 [int(yCenter_SAX + targetImg_size_SAX[1] // 2), dims[1]]),
								 :], dtype='float32')

		if cropImage_SAX.shape[0] != cropImage_SAX.shape[1]:
			print('SAX Target shape is ', targetImg_size_SAX, ', final shape is ', cropImage_SAX.shape)
			print('Getting full image in sax plane')
			xCenter_SAX = data_input.shape[0] // 2
			yCenter_SAX = data_input.shape[1] // 2
			targetImg_size_SAX = data_input.shape[:2]

			cropImage_SAX = np.array(
				data_input[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
				int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
				:], dtype='float32')

		if counter == 0:
			par_file = input_image_folder + '/XCAT_parameters'
			with open(par_file, 'r') as infile:
				xcat_par = infile.readlines()
			resx = spacing[0] * 100.0
			resz = spacing[2] * 100.0
			for lcounter, l_sel in enumerate(xcat_par):
				if l_sel[:11] == ' array_size':
					xcat_par[lcounter] = ' array_size = %d\n' % (cropImage_SAX.shape[0])
				elif l_sel[:9] == ' starting':
					xcat_par[lcounter] = ' starting slice number    = 0\n'
				elif l_sel[:7] == ' ending':
					xcat_par[lcounter] = ' ending slice number      = %d\n' % (
					cropImage_SAX.shape[2])  # there is a weird +1 in MRXCAT
				elif l_sel[:14] == ' pixel width =':
					xcat_par[lcounter] = ' pixel width = %2.4f (cm/pixel)\n' % (resx)  # there is a weird +1 in MRXCAT
				elif l_sel[:14] == ' slice width =':
					xcat_par[lcounter] = ' slice width = %2.4f (cm/pixel)\n' % (resz)  # there is a weird +1 in MRXCAT
				elif l_sel[:13] == ' voxel volume':
					xcat_par[lcounter] = ' voxel volume (pixel width^2)(slice_width) =  %2.4f ml\n' % (
								spacing[1] * spacing[2] * spacing[0] * 1e6)

			with open(results_folder_SAX + '/Synthetic_image_log', 'w') as outfile:
				for l_sel in xcat_par:
					outfile.write(l_sel)

		cropImage_SAX_PD = np.array(
			PD[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')
		cropImage_SAX_T1 = np.array(
			T1[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')
		cropImage_SAX_T2 = np.array(
			T2[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')
		cropImage_SAX_T2s = np.array(
			T2s[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')
		cropImage_SAX_radial = np.array(
			e_radial[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')
		cropImage_SAX_circ = np.array(e_circ[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')
		cropImage_SAX_mask = np.array(LV_mask[int(xCenter_SAX - targetImg_size_SAX[0] // 2):int(xCenter_SAX + targetImg_size_SAX[0] // 2),
			int(yCenter_SAX - targetImg_size_SAX[1] // 2):int(yCenter_SAX + targetImg_size_SAX[1] // 2),
			:], dtype='float32')

		plot_all = np.concatenate((cropImage_SAX_PD[:, :, 0] / cropImage_SAX_PD[:, :, 0].max(),
								   cropImage_SAX_T1[:, :, 0] / cropImage_SAX_T1[:, :, 0].max(),
								   cropImage_SAX_T2[:, :, 0] / cropImage_SAX_T2[:, :, 0].max()), 1)
		plt.imshow(plot_all)
		plt.savefig(results_folder_SAX + '/CheckFrame_%02d.png' % (counter,), bbox_inches='tight', dpi=400)
		plt.close()

		f = open(results_folder_SAX + '/Image_new_origin.txt', 'w')
		f.write('%1.4f ' % (orig_image[0] + int(xCenter_SAX - targetImg_size_SAX[0] // 2) * spacing[0]))
		f.write('%1.4f ' % (orig_image[1] + int(yCenter_SAX - targetImg_size_SAX[1] // 2) * spacing[1]))
		f.write('%1.4f ' % (orig_image[2]))
		f.close()

		cropImage_SAX = np.ceil(cropImage_SAX).reshape(-1, 1, order='F')
		cropImage_SAX.tofile(results_folder_SAX + '/Synthetic_image_act_%d.bin' % (counter + 1,))

		cropImage_SAX_PD = cropImage_SAX_PD.reshape(-1, 1, order='F')
		cropImage_SAX_PD.tofile(results_folder_SAX + '/PD/Synthetic_image_act_%d.bin' % (counter + 1,))

		cropImage_SAX_T1 = cropImage_SAX_T1.reshape(-1, 1, order='F')
		cropImage_SAX_T1.tofile(results_folder_SAX + '/T1/Synthetic_image_act_%d.bin' % (counter + 1,))

		cropImage_SAX_T2 = cropImage_SAX_T2.reshape(-1, 1, order='F')
		cropImage_SAX_T2.tofile(results_folder_SAX + '/T2/Synthetic_image_act_%d.bin' % (counter + 1,))

		cropImage_SAX_T2s = cropImage_SAX_T2s.reshape(-1, 1, order='F')
		cropImage_SAX_T2s.tofile(results_folder_SAX + '/T2s/Synthetic_image_act_%d.bin' % (counter + 1,))

		np.save(results_folder_SAX + '/SAX_radial_GT_%d.npy' % (counter + 1,), cropImage_SAX_radial)
		np.save(results_folder_SAX + '/SAX_circ_GT_%d.npy' % (counter + 1,), cropImage_SAX_circ)
		np.save(results_folder_SAX + '/SAX_mask_GT_%d.npy' % (counter + 1,), cropImage_SAX_mask)

	return 0
