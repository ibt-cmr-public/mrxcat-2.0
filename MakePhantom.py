'''
- Generation of XCAT phantom
- Conversion of .bin files to VTI images 
- Export displacement fields in VTI format

(c) 2022, Dr. Buoso Stefano
buoso@biomed.ee.ethz.ch
'''

import numpy as np
import os, shutil
import sys

import vtk
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
import matplotlib.pyplot as plt

from scipy.interpolate import RegularGridInterpolator
import cv2

sys.path.append('myTexturizer')
from texturizationFunctions import defineTissueProperties, defineTissuePropertiesMRXCAT, fixLVTexture, textureReport, \
    XCATProperties

sys.path.append('myWarpFunctions')
import myMorphingFunctions as MMF


class myLabels:
    def __init__(self, LV_wall, RV_wall, LV_blood, RV_blood, Peri, Aorta):
        self.LV_wall = LV_wall
        self.RV_wall = RV_wall
        self.LV_blood = LV_blood
        self.RV_blood = RV_blood
        self.Peri = Peri
        self.Aorta = Aorta


XCAT_values = XCATProperties()

def get_values_mapping(s):
    varstring = ''.join((ch if ch in '0123456789.' else ' ') for ch in s)
    listOfN = [float(i) for i in varstring.split()]
    return listOfN

runXCAT      = False
runWarper    = True
runConverter = True

# Input section XCAT model
case_name           = 'Background'
xcat_img_folder     = '/inputData/'
par_file_folder     = '/inputData/'
param_file          = 'XCAT_par_file.par'
output_caseXCAT     = xcat_img_folder + '/'+ case_name

target_heart_phases = 20

# Input section personalized phantom
patient_name     = 'Patient1'
selec_suffix     = '/'
input_image_case = case_name
out_folder_synth_images = '/outputData/'
template_im_path        = xcat_img_folder
warp_with_beating   = True
warp_with_breathing = False
use_texturizer      = True
wrap_texture_only   = False  # If false recompute the one in the BP
textureLV           = False
targetImg_size_bin = [250,250,1]

#House keepping
output_folder_bin  = out_folder_synth_images + '/'+patient_name + '/Phamtom4MRXCAT_SAX/'
input_image_motion_folder = template_im_path + '/' + input_image_case + '/XCAT_vti'
results_folder = out_folder_synth_images + '/' + patient_name + selec_suffix
warped_images = results_folder + '/XCAT_warped_vti/'
Def_folder = out_folder_synth_images + '/' + patient_name + selec_suffix + '/VTK/Displacement/'
network_path = os.getcwd() + '/myTexturizer/LAXsegnet'

if not os.path.exists(results_folder):
    os.makedirs(results_folder)

if not os.path.exists(results_folder + '/SAX_slices/'):
    os.makedirs(results_folder + '/SAX_slices/')

if not os.path.exists(warped_images):
    os.makedirs(warped_images)

if not os.path.exists(results_folder + '/Tissue_properties_initial_frame/'):
    os.makedirs(results_folder + '/Tissue_properties_initial_frame/')

if not os.path.exists(results_folder + '/VTK_shifted/'):
    os.makedirs(results_folder + '/VTK_shifted/')

shutil.copyfile(template_im_path + '/' + input_image_case + '/XCAT_output/Image_log',
                results_folder + '/XCAT_parameters')

if runXCAT:
    MMF. run_MRXCAT(param_file,target_heart_phases,par_file_folder,output_caseXCAT)

if runWarper:
    maskLabels = myLabels(1, 2, 5, 6, 50, 36)

    # Reading frames and ensuring consistency of frames
    frames_0 = np.sort(next(os.walk(Def_folder))[2])
    frames = []
    for frame_sel in frames_0:
        if frame_sel[0] != '.':
            if frame_sel[-3:] == 'vtk':
                frames.append(frame_sel)

    initial_FEM = frames[0]

    # Read reference image for scaling and dimension acquisition
    read_img = vtk.vtkXMLImageDataReader()
    read_img.SetFileName(input_image_motion_folder + '/Image_%02d.vti' % (1,))
    read_img.Update()
    img_ref = read_img.GetOutput()
    spacing, orig_image = MMF.computeScalingImage(Def_folder + initial_FEM,
                                                  input_image_motion_folder + '/Image_%02d.vti' % (1,), maskLabels)

    img_ref = MMF.scaleImage(img_ref, spacing, orig_image)

    dims = img_ref.GetDimensions()
    orig_image = img_ref.GetOrigin()
    spacing = img_ref.GetSpacing()
    data_input = vtk_to_numpy(img_ref.GetPointData().GetArray('labels')).reshape(dims[0], dims[1], dims[2], order="F")

    # Read initial reference mesh
    displ_reader = vtk.vtkUnstructuredGridReader()
    displ_reader.ReadAllScalarsOn()
    displ_reader.ReadAllVectorsOn()
    displ_reader.SetFileName(Def_folder + initial_FEM)
    displ_reader.Update()
    mesh = displ_reader.GetOutput()

    mesh_pt = mesh.GetNumberOfPoints()

    probe = vtk.vtkProbeFilter()
    probe.SetInputData(img_ref)
    probe.SetSourceData(mesh)
    probe.Update()
    probed_image = probe.GetOutput()

    Target_mask = vtk_to_numpy(probed_image.GetPointData().GetArray('vtkValidPointMask')).reshape(dims[0], dims[1], dims[2],
                                                                                                  order="F")
    # Find pixels related to apex to compute displacements
    apex_z = np.min(np.where(Target_mask == 1)[2])
    apex_x_, apex_y_ = np.where(Target_mask[:, :, apex_z] == 1)
    apex_x = np.mean(apex_x_)
    apex_y = np.mean(apex_y_)

    # Compute mean slice for image generation
    SAX_slice_selection = int(np.mean(np.where(Target_mask == 1)[2]))

    data_input, Target_mask, BP_fill, mapx0,mapy0,centerBox_x,centerBox_y = MMF.warpSlice(Target_mask,data_input,SAX_slice_selection,maskLabels,True)

    img_ref = MMF.vtkSliceImage(data_input,spacing,[orig_image[0],orig_image[1],orig_image[2]+SAX_slice_selection*spacing[2]])

    img_ref = MMF.addArrayToVtk(img_ref, data_input, 'labels', False)
    img_ref = MMF.addArrayToVtk(img_ref, Target_mask, 'LV_mask', False)

    # Add texturization of current image warped to adapt to FEM model
    if use_texturizer:
        PD0, T10, T20, T2s0 = defineTissueProperties(img_ref, network_path)
        if not textureLV:
            PD0, T10, T20, T2s0, LV_prop_vals = fixLVTexture(data_input, PD0, T10, T20, T2s0, maskLabels,
                                                             which_prop='meanLV')
    else:
        PD0, T10, T20, T2s0, LV_prop_vals = defineTissuePropertiesMRXCAT(data_input)

    ind_LV_wall = np.where(data_input == maskLabels.LV_wall)
    ind_LV_BP   = np.where(data_input == maskLabels.LV_blood)

    par_img = img_ref
    for arr_n in ['dx_beating', 'dy_beating', 'dz_beating', 'dx_breathing', 'dy_breathing', 'dz_breathing', 'maskValues',
                  'parameters']:
        par_img.GetPointData().RemoveArray(arr_n)
        par_img.Modified()

    par_img = MMF.addArrayToVtk(par_img, PD0, 'parameters', False)
    MMF.saveVTI(par_img,results_folder + '/Tissue_properties_initial_frame/PD.vti')

    par_img = MMF.addArrayToVtk(par_img, T10, 'parameters', False)
    MMF.saveVTI(par_img,results_folder + '/Tissue_properties_initial_frame/T1.vti')

    par_img = MMF.addArrayToVtk(par_img, T20, 'parameters', False)
    MMF.saveVTI(par_img,results_folder + '/Tissue_properties_initial_frame/T2.vti')

    par_img = MMF.addArrayToVtk(par_img, T2s0, 'parameters', False)
    MMF.saveVTI(par_img,results_folder + '/Tissue_properties_initial_frame/T2s.vti')

    PD_mesh  = LV_prop_vals[0] * np.ones((mesh_pt, 1))
    T1_mesh  = LV_prop_vals[1] * np.ones((mesh_pt, 1))
    T2_mesh  = LV_prop_vals[2] * np.ones((mesh_pt, 1))
    T2s_mesh = LV_prop_vals[3] * np.ones((mesh_pt, 1))

    mesh = MMF.addArrayToVtk(mesh, PD_mesh ,'PD', False)
    mesh = MMF.addArrayToVtk(mesh, T1_mesh, 'T1', False)
    mesh = MMF.addArrayToVtk(mesh, T2_mesh, 'T2', False)
    mesh = MMF.addArrayToVtk(mesh, T2s_mesh, 'T2', False)

    PD_mesh = numpy_to_vtk(PD_mesh)
    PD_mesh.SetName('PD')
    T1_mesh = numpy_to_vtk(T1_mesh)
    T1_mesh.SetName('T1')
    T2_mesh = numpy_to_vtk(T2_mesh)
    T2_mesh.SetName('T2')
    T2s_mesh = numpy_to_vtk(T2s_mesh)
    T2s_mesh.SetName('T2s')

    writer = vtk.vtkUnstructuredGridWriter()
    writer.SetInputData(mesh)
    writer.SetFileName(results_folder + '/Tissue_properties_initial_frame/Texture_on_mesh.vtk')
    writer.Write()

    n_image_points = img_ref.GetNumberOfPoints()
    X0 = np.zeros((n_image_points, 3))
    for i in range(n_image_points):
        X0[i, :] = img_ref.GetPoint(i)

    x_grid = np.sort(np.unique(X0[:, 0]))
    y_grid = np.sort(np.unique(X0[:, 1]))
    z_grid = np.sort(np.unique(X0[:, 2]))

    MASK_interp = RegularGridInterpolator((x_grid, y_grid, z_grid), data_input, method='nearest', bounds_error=False,
                                          fill_value=1.0)
    # Create interpolators for image warping
    PD_interp  = RegularGridInterpolator((x_grid, y_grid, z_grid), PD0, method='nearest', bounds_error=False, fill_value=1.0)
    T1_interp  = RegularGridInterpolator((x_grid, y_grid, z_grid), T10, method='nearest', bounds_error=False, fill_value=1.0)
    T2_interp  = RegularGridInterpolator((x_grid, y_grid, z_grid), T20, method='nearest', bounds_error=False, fill_value=1.0)
    T2s_interp = RegularGridInterpolator((x_grid, y_grid, z_grid), T2s0, method='nearest', bounds_error=False,
                                         fill_value=1.0)

    img_ref = MMF.addArrayToVtk(img_ref, data_input, 'labels', False)
    img_ref = MMF.addArrayToVtk(img_ref, PD0, 'PD', False)
    img_ref = MMF.addArrayToVtk(img_ref, T10, 'T1', False)
    img_ref = MMF.addArrayToVtk(img_ref, T20, 'T2', False)
    img_ref = MMF.addArrayToVtk(img_ref, T2s0, 'T2s', False)

    img_ref = MMF.addArrayToVtk(img_ref, np.array(Target_mask * 0), 'e_circ', False)
    img_ref = MMF.addArrayToVtk(img_ref, np.array(Target_mask * 0), 'e_radial', False)
    img_ref = MMF.addArrayToVtk(img_ref, np.array(Target_mask * 0), 'e_long', False)

    for arr_n in ['dx_beating', 'dy_beating', 'dz_beating', 'dx_breathing', 'dy_breathing', 'dz_breathing', 'maskValues',
                  'parameters']:
        img_ref.GetPointData().RemoveArray(arr_n)
        img_ref.Modified()
    MMF.saveVTI(img_ref,warped_images + '/Image_%02d.vti' % (1,))

    plt.imshow(data_input[:, :, 0], alpha=0.8)
    plt.imshow(Target_mask[:, :, 0], cmap=plt.cm.gray, alpha=0.2)
    plt.savefig(results_folder + '/SAX_slices/Frame_%02d.png' % (1,), bbox_inches='tight', dpi=400)
    plt.close()

    data_input_0 = data_input.copy()

    for counter, frame_sel in enumerate(frames[1:]):

        print('Warping phase ', frame_sel)

        # Read frame from XCAT
        read_img.SetFileName(input_image_motion_folder + '/Image_%02d.vti' % (counter + 2,))
        read_img.Update()
        img_ref = read_img.GetOutput()
        img_ref = MMF.scaleImage(img_ref, spacing, orig_image)

        # Read mesh
        # Read initial, reference mesh and append tissue properties
        displ_reader = vtk.vtkUnstructuredGridReader()
        displ_reader.ReadAllScalarsOn()
        displ_reader.ReadAllVectorsOn()
        displ_reader.SetFileName(Def_folder + frames[counter])
        displ_reader.Update()
        mesh = displ_reader.GetOutput()

        disp_field_name = 'displacement'
        displGT = vtk_to_numpy(mesh.GetPointData().GetArray(disp_field_name))

        mesh.GetPointData().AddArray(PD_mesh)
        mesh.Modified()
        mesh.GetPointData().AddArray(T1_mesh)
        mesh.Modified()
        mesh.GetPointData().AddArray(T2_mesh)
        mesh.Modified()
        mesh.GetPointData().AddArray(T2s_mesh)
        mesh.Modified()

        max_displGT_x = np.max(abs(displGT[:, 0]))
        max_displGT_y = np.max(abs(displGT[:, 1]))
        max_displGT_z = np.max(abs(displGT[:, 2]))

        if warp_with_beating:

            displx_beat = spacing[0] * vtk_to_numpy(img_ref.GetPointData().GetArray('dx_beating')).reshape(dims[0], dims[1],
                                                                                                           dims[2],
                                                                                                           order="F")
            disply_beat = spacing[1] * vtk_to_numpy(img_ref.GetPointData().GetArray('dy_beating')).reshape(dims[0], dims[1],
                                                                                                           dims[2],
                                                                                                           order="F")
            displz_beat = spacing[2] * vtk_to_numpy(img_ref.GetPointData().GetArray('dz_beating')).reshape(dims[0], dims[1],
                                                                                                           dims[2],
                                                                                                           order="F")
            displx_beat = cv2.remap(np.array(displx_beat[:, :, SAX_slice_selection], dtype='float32'),
                                              np.array(mapx0[...,0], dtype='float32'),
                                              np.array(mapy0[...,0], dtype='float32'), cv2.INTER_NEAREST)
            disply_beat = cv2.remap(np.array(disply_beat[:, :, SAX_slice_selection], dtype='float32'),
                                              np.array(mapx0[...,0], dtype='float32'),
                                              np.array(mapy0[...,0], dtype='float32'), cv2.INTER_NEAREST)
            displz_beat = cv2.remap(np.array(displz_beat[:, :, SAX_slice_selection], dtype='float32'),
                                              np.array(mapx0[...,0], dtype='float32'),
                                                  np.array(mapy0[...,0], dtype='float32'), cv2.INTER_NEAREST)

            if np.max(abs(displx_beat)) > 0:
                displx = displx_beat * (max_displGT_x / (np.max(abs(displx_beat))))
            if np.max(abs(disply_beat)) > 0:
                disply = disply_beat * (max_displGT_y / (np.max(abs(disply_beat))))
            if np.max(abs(displz_beat)) > 0:
                displz = displz_beat * (max_displGT_z / (np.max(abs(displz_beat))))

        else:
            displx = np.zeros(data_input.shape)
            disply = np.zeros(data_input.shape)
            displz = np.zeros(data_input.shape)

        if warp_with_breathing:
            displx_breat = spacing[0] * vtk_to_numpy(img_ref.GetPointData().GetArray('dx_breathing')).reshape(dims[0],
                                                                                                              dims[1],
                                                                                                              dims[2],
                                                                                                              order="F")
            disply_breat = spacing[1] * vtk_to_numpy(img_ref.GetPointData().GetArray('dy_breathing')).reshape(dims[0],
                                                                                                              dims[1],
                                                                                                              dims[2],
                                                                                                              order="F")
            displz_breat = spacing[2] * vtk_to_numpy(img_ref.GetPointData().GetArray('dz_breathing')).reshape(dims[0],
                                                                                                              dims[1],
                                                                                                              dims[2],
                                                                                                              order="F")

            displx_breat = cv2.remap(np.array(displx_breat[:, :, SAX_slice_selection], dtype='float32'),
                                               np.array(mapx0[:, :, 0], dtype='float32'),
                                               np.array(mapy0[:, :, 0], dtype='float32'), cv2.INTER_LINEAR)
            disply_breat = cv2.remap(np.array(disply_breat[:, :, SAX_slice_selection], dtype='float32'),
                                               np.array(mapx0[:, :, 0], dtype='float32'),
                                               np.array(mapy0[:, :, 0], dtype='float32'), cv2.INTER_LINEAR)
            displz_breat = cv2.remap(np.array(displz_breat[:, :, SAX_slice_selection], dtype='float32'),
                                               np.array(mapx0[:, :, 0], dtype='float32'),
                                               np.array(mapy0[:, :, 0], dtype='float32'), cv2.INTER_LINEAR)

            displ_apex_x = displx_breat[int(apex_x), int(apex_y), int(apex_z)]
            displ_apex_y = disply_breat[int(apex_x), int(apex_y), int(apex_z)]
            displ_apex_z = displz_breat[int(apex_x), int(apex_y), int(apex_z)]

            print(displ_apex_x, displ_apex_y, displ_apex_z)

            displx += displx_breat
            disply += disply_breat
            displz += displz_breat

        if warp_with_beating or warp_with_breathing:

            displx = displx.reshape(-1, 1, order="F")
            disply = disply.reshape(-1, 1, order="F")
            displz = displz.reshape(-1, 1, order="F")

            # Given the displacements, a point in the new image will have the properties of the points that were positioned at -displ before
            x_to_look_for = X0.astype('float32')
            x_to_look_for[:, 0] -= displx[:, 0]
            x_to_look_for[:, 1] -= disply[:, 0]
            x_to_look_for[:, 2] -= displz[:, 0]

            x_to_look_for[x_to_look_for[:, 0] > X0[:, 0].max(), 0] = X0[:, 0].max()
            x_to_look_for[x_to_look_for[:, 0] < X0[:, 0].min(), 0] = X0[:, 0].min()
            x_to_look_for[x_to_look_for[:, 1] > X0[:, 1].max(), 1] = X0[:, 1].max()
            x_to_look_for[x_to_look_for[:, 1] < X0[:, 1].min(), 1] = X0[:, 1].min()
            x_to_look_for[x_to_look_for[:, 2] > X0[:, 2].max(), 2] = X0[:, 2].max()
            x_to_look_for[x_to_look_for[:, 2] < X0[:, 2].min(), 2] = X0[:, 2].min()

            new_mask = np.rint(MASK_interp(x_to_look_for))
            new_pd = PD_interp(x_to_look_for)
            new_t1 = T1_interp(x_to_look_for)
            new_t2 = T2_interp(x_to_look_for)
            new_t2s = T2s_interp(x_to_look_for)

            data_input = new_mask.reshape(dims[0], dims[1], 1, order="F")
            PD = new_pd.reshape(dims[0], dims[1], 1, order="F")
            T1 = new_t1.reshape(dims[0], dims[1], 1, order="F")
            T2 = new_t2.reshape(dims[0], dims[1], 1, order="F")
            T2s = new_t2s.reshape(dims[0], dims[1], 1, order="F")

        else:

            PD = PD0.copy()
            T1 = T10.copy()
            T2 = T20.copy()
            T2s = T2s0.copy()
            data_input = data_input_0

        if warp_with_breathing:
            displGT[:, 0] = displGT[:, 0] + displ_apex_x
            displGT[:, 1] = displGT[:, 1] + displ_apex_y
            displGT[:, 2] = displGT[:, 2] + displ_apex_z

        intensity_vtk = numpy_to_vtk(displGT)
        intensity_vtk.SetName('imgDispl')
        intensity_vtk.SetNumberOfComponents(3)
        mesh.GetPointData().AddArray(intensity_vtk)
        mesh.Modified()

        if warp_with_breathing:
            writer = vtk.vtkUnstructuredGridWriter()
            writer.SetInputData(mesh)
            writer.SetFileName(results_folder + '/VTK_shifted/Displ_%02d.vtk' % (counter + 2,))
            writer.Write()

        mesh.GetPointData().SetActiveVectors('imgDispl')
        vtk_warp = vtk.vtkWarpVector()
        vtk_warp.SetInputData(mesh)
        vtk_warp.Update()
        warped_mesh = vtk_warp.GetOutput()

        probe = vtk.vtkProbeFilter()
        probe.SetInputData(img_ref)
        probe.SetSourceData(warped_mesh)
        probe.Update()
        probed_image = probe.GetOutput()

        Target_mask = vtk_to_numpy(probed_image.GetPointData().GetArray('vtkValidPointMask')).reshape(dims[0], dims[1],
                                                                                                      dims[2], order="F")
        PD_LV = vtk_to_numpy(probed_image.GetPointData().GetArray('PD')).reshape(dims[0], dims[1], dims[2], order="F")
        T1_LV = vtk_to_numpy(probed_image.GetPointData().GetArray('T1')).reshape(dims[0], dims[1], dims[2], order="F")
        T2_LV = vtk_to_numpy(probed_image.GetPointData().GetArray('T2')).reshape(dims[0], dims[1], dims[2], order="F")
        T2s_LV = vtk_to_numpy(probed_image.GetPointData().GetArray('T2s')).reshape(dims[0], dims[1], dims[2], order="F")

        e_r = vtk_to_numpy(probed_image.GetPointData().GetArray('e_radial')).reshape(dims[0], dims[1], dims[2], order="F")
        e_c = vtk_to_numpy(probed_image.GetPointData().GetArray('e_circ')).reshape(dims[0], dims[1], dims[2], order="F")
        e_l = vtk_to_numpy(probed_image.GetPointData().GetArray('e_long')).reshape(dims[0], dims[1], dims[2], order="F")

        data_input, Target_mask, BP_fill, mapx, mapy, centerBox_x, centerBox_y = MMF.warpSlice(Target_mask, data_input, SAX_slice_selection,
                                                                           maskLabels, False)

        img_ref = MMF.vtkSliceImage(data_input, spacing,
                                    [orig_image[0], orig_image[1], orig_image[2] + SAX_slice_selection * spacing[2]])


        PD = cv2.remap(np.array(PD[...,0], dtype='float32'), np.array(mapx[...,0], dtype='float32'),
                                 np.array(mapy[...,0], dtype='float32'), cv2.INTER_LINEAR)

        T1 = cv2.remap(np.array(T1[...,0], dtype='float32'), np.array(mapx[...,0], dtype='float32'),
                                 np.array(mapy[...,0], dtype='float32'), cv2.INTER_LINEAR)
        T2 = cv2.remap(np.array(T2[...,0], dtype='float32'), np.array(mapx[...,0], dtype='float32'),
                                 np.array(mapy[...,0], dtype='float32'), cv2.INTER_LINEAR)
        T2s = cv2.remap(np.array(T2s[...,0], dtype='float32'), np.array(mapx[...,0], dtype='float32'),
                                  np.array(mapy[...,0], dtype='float32'), cv2.INTER_LINEAR)

        img_ref = MMF.vtkSliceImage(data_input, spacing,
                                    [orig_image[0], orig_image[1], orig_image[2] + SAX_slice_selection * spacing[2]])

        img_ref = MMF.addArrayToVtk(img_ref,data_input,'labels',False)

        if not wrap_texture_only:
            if use_texturizer:
                PD_bp, T1_bp, T2_bp, T2s_bp = defineTissueProperties(img_ref, network_path)
            else:
                PD_bp, T1_bp, T2_bp, T2s_bp, _ = defineTissuePropertiesMRXCAT(data_input)

            if use_texturizer:
                PD = np.multiply(1 - BP_fill[...,0], PD) + np.multiply(BP_fill[...,0], PD_bp[...,0])
                T1 = np.multiply(1 - BP_fill[...,0], T1) + np.multiply(BP_fill[...,0], T1_bp[...,0])
                T2 = np.multiply(1 - BP_fill[...,0], T2) + np.multiply(BP_fill[...,0], T2_bp[...,0])
                T2s = np.multiply(1 - BP_fill[...,0], T2s) + np.multiply(BP_fill[...,0], T2s_bp[...,0])
            else:
                PD = np.multiply(1 - BP_fill[...,0], PD[...,0]) + np.multiply(BP_fill[...,0], XCAT_values.blood[0])
                T1 = np.multiply(1 - BP_fill[...,0], T1[...,0]) + np.multiply(BP_fill[...,0], XCAT_values.blood[1])
                T2 = np.multiply(1 - BP_fill[...,0], T2[...,0]) + np.multiply(BP_fill[...,0], XCAT_values.blood[2])
                T2s = np.multiply(1 - BP_fill[...,0], T2s[...,0]) + np.multiply(BP_fill[...,0], XCAT_values.blood[2])

        PD = np.multiply(1 - Target_mask[...,0], PD) + np.multiply(Target_mask[...,0], LV_prop_vals[0])
        T1 = np.multiply(1 - Target_mask[...,0], T1) + np.multiply(Target_mask[...,0], LV_prop_vals[1])
        T2 = np.multiply(1 - Target_mask[...,0], T2) + np.multiply(Target_mask[...,0], LV_prop_vals[2])
        T2s = np.multiply(1 - Target_mask[...,0], T2s) + np.multiply(Target_mask[...,0], LV_prop_vals[3])

        # Interpolation sometimes fails, so I need to correct T2 that sometimes go to zero
        id0 = np.where(T2 == 0)
        T2[id0] = XCAT_values.average[2]
        T1[id0] = XCAT_values.average[1]
        PD[id0] = XCAT_values.average[0]

        plt.imshow(data_input[:, :, 0], alpha=0.8)
        plt.imshow(Target_mask[:, :, 0], cmap=plt.cm.gray, alpha=0.2)
        plt.savefig(results_folder + '/SAX_slices/Frame_%02d.png' % (counter + 2,), bbox_inches='tight', dpi=400)
        plt.close()

        img_ref = MMF.addArrayToVtk(img_ref,PD,'PD',False)
        img_ref = MMF.addArrayToVtk(img_ref,T1,'T1',False)
        img_ref = MMF.addArrayToVtk(img_ref,T2,'T2',False)
        img_ref = MMF.addArrayToVtk(img_ref,T2s,'T2s',False)

        for arr_n in ['dx_beating', 'dy_beating', 'dz_beating', 'dx_breathing', 'dy_breathing', 'dz_breathing',
                      'maskValues', 'parameters']:
            img_ref.GetPointData().RemoveArray(arr_n)
            img_ref.Modified()

        img_ref = MMF.addArrayToVtk(img_ref,e_c[...,SAX_slice_selection],'e_circ',False)
        img_ref = MMF.addArrayToVtk(img_ref,e_l[...,SAX_slice_selection],'e_long',False)
        img_ref = MMF.addArrayToVtk(img_ref,e_r[...,SAX_slice_selection],'e_radial',False)

        img_ref = MMF.addArrayToVtk(img_ref,PD_LV[...,SAX_slice_selection],'PD_LV',False)
        img_ref = MMF.addArrayToVtk(img_ref,T1_LV[...,SAX_slice_selection],'T1_LV',False)
        img_ref = MMF.addArrayToVtk(img_ref,T2_LV[...,SAX_slice_selection],'T2_LV',False)
        img_ref = MMF.addArrayToVtk(img_ref,T2s_LV[...,SAX_slice_selection],'T2s_LV',False)
        img_ref = MMF.addArrayToVtk(img_ref,Target_mask,'LV_mask',False)

        MMF.saveVTI(img_ref, warped_images + '/Image_%02d.vti' % (counter + 2,))

if runConverter:
    MMF.vti2Bin(results_folder, output_folder_bin, targetImg_size_bin)
