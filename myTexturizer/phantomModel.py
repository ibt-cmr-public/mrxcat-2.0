import os
import imageio
import numpy as np
from scipy.ndimage import distance_transform_edt as dtf

mask_colors = {
	'ventricle_right':    np.array([230,  25,  75, 255]),    # e6194b
	'ventricle_left':     np.array([240,  50, 230, 255]),    # f032e6
	'myocardium_right':   np.array([ 67,  99, 216, 255]),    # 4363d8
	'myocardium_left':    np.array([ 66, 212, 244, 255]),    # 42d4f4
	'outside':            np.array([191, 239,  69, 255]),    # bfef45
	'lungs':              np.array([ 60, 180,  75, 255]),    # 3cb44b
	'liver':              np.array([255, 225,  25, 255]),    # ffe119
	'fat_around_heart':   np.array([245, 130,  49, 255]),    # f58231
	'skin_fat':           np.array([145,  30, 180, 255]),    # 911eb4
	'stomach':            np.array([128,   0,   0, 255]),    # 800000
	'ribs':               np.array([  9,   0, 164, 255]),    # 0900a4
	'unknown':            np.array([  0,   0,   0,   0]),    # 000000
}

tissue_property_values = {}

pd_max, t1_max, t2_max, t2star_max = 100.,1400.,285.,70.

# # pd_max, t1_max, t2_max, t2star_max = 90.,1400.,285.,70.
muscle = 	np.array([88./pd_max, 1000./t1_max, 43./t2_max,  28./t2star_max])
fat = 		np.array([60./pd_max, 250./t1_max,  70./t2_max,  39./t2star_max])
blood = 	np.array([79./pd_max, 1387./t1_max, 280./t2_max, 66./t2star_max])
liver = 	np.array([87./pd_max, 661./t1_max,  57./t2_max,  34./t2star_max])
bone = 		np.array([71./pd_max, 250./t1_max,  20./t2_max,  1./t2star_max])
unknown = 	np.array([65./pd_max, 750./t1_max,  60/t2_max,   30./t2star_max])
air = 		np.array([34./pd_max, 1171./t1_max, 61./t2_max,  1./t2star_max])
tissue_property_values['ours'] = [muscle, blood, air, liver, fat, bone, unknown]

# pd_max, t1_max, t2_max, t2star_max = 100.,1300.,105.,55.
muscle = 	np.array([80./pd_max, 900./t1_max,  50./t2_max,  31./t2star_max])
fat = 		np.array([70./pd_max, 350./t1_max,  30./t2_max,  20./t2star_max])
blood = 	np.array([95./pd_max, 1200./t1_max, 100./t2_max, 50./t2star_max])
liver = 	np.array([90./pd_max, 800./t1_max,  50./t2_max,  31./t2star_max])
bone = 		np.array([12./pd_max, 250./t1_max,  20./t2_max,  1./t2star_max])
average = 	np.array([70./pd_max, 700./t1_max,  50/t2_max,   30./t2star_max])
air = 		np.array([34./pd_max, 1171./t1_max, 61./t2_max,  1./t2star_max])
tissue_property_values['xcat'] = [muscle, blood, air, liver, fat, bone, unknown]

class Phantom():

	#0 = muscle
	#1 = blood
	#2 = air
	#3 = liver
	#4 = fat
	#5 = bone
	#6 = unknown

	def __init__(self, arr, image=None, convert_from=None ):

		if convert_from is None: #guess the format
			if np.max(arr) <= 100:
				convert_from = 'xcat'
			else:
				convert_from = 'dg'

		if convert_from.lower() == 'xcat': #many-class array from XCAT
			muscle_mask = np.sum([arr==i for i in [1,2,3,4,10]], axis=0)
			blood_mask = np.sum([arr==i for i in [5,6,7,8]], axis=0)#36,37
			air_mask = np.sum([arr==i for i in [0,15,16]], axis=0)
			liver_mask = np.sum([arr==i for i in [13,40,41,42,43,52]], axis=0) #-52
			fat_mask = np.sum([arr==i for i in [50,99]], axis=0)
			bone_mask = np.sum([arr==i for i in [31,32,33,34,35,51]], axis=0)
			known_mask = air_mask + muscle_mask + blood_mask + bone_mask + fat_mask + liver_mask
			unknown_mask = np.ones(arr.shape) - known_mask

			new_arr = np.zeros(arr.shape)
			arr = blood_mask*1 + air_mask*2 + liver_mask*3 + fat_mask*4 + bone_mask*5 + unknown_mask*6
			
		if convert_from.lower() == 'dg': #RGB image resulting from Nico's DatasetGAN
			pass

		#the array should contain only the known classes (but might not contain all of them)
		for val in np.unique(arr.astype('int')):
			assert val in [0,1,2,3,4,5,6]

		self.phantom = (arr+0).astype('int')
		self.real_image = image

	# def addSkinFat(self,):
	# 	self.phantom = addSkinFat(self.phantom)

	def getTissuePropertyMap(self, property_values_to_use='ours'):

		assert property_values_to_use in tissue_property_values.keys()

		properties = tissue_property_values[property_values_to_use]

		s_params = np.zeros(self.phantom.shape+(4,),dtype='float')
		for c in range(7):
			s_params += (self.phantom[...,None]==c) * properties[c]

		return s_params

	def simpleSignalModel(self, sequence='bssfp', property_values_to_use='ours', TR=4, TE=2, flip_angle=0.35):

		tpm = self.getTissuePropertyMap(property_values_to_use)
		return simpleSignalModel(tpm, TR=TR, TE=TE, flip_angle=flip_angle, mode=sequence)

	def toRGB(self,):
		pass




class XCATPhantom():

	def __init__( x ):

		self.original_phantom = x+0
		self.phantom = None
		self.real_image = None

	# def 



# our_class_to_xcat_class = {
# 	'ventricle_right':    5,
# 	'ventricle_left':     5,
# 	'myocardium_right':   1,
# 	'myocardium_left':    1,
# 	'outside':            0,
# 	'lungs':              0,
# 	'liver':              13,
# 	'fat_around_heart':   50,
# 	'skin_fat':           50,
# 	'stomach':            13,
# 	'ribs':               32,
# 	'unknown':            -1,
# }


def addSkinFat(a, random=False):
	'''
	adds a (potentially random thickness) "fat" layer around the phantom.
	input should be an xcat phantom array with integer class labels
	'''

	outside = a == 0
	body = a == 9
	if random:
		fat_thickness = np.random.random()*25
	else:
		fat_thickness = 12
	r = (dtf(1-outside) < fat_thickness) * (1-outside)
	mask = r*body
	a = a*(1-mask)+mask*99
	return a

"""
def getTissueProperties(arr, params_to_use='ours'):
	'''
	takes a xcat phantom as input with each pixel/voxel having an integer class, 
	returns an array with [PD,T1,T2*] for each pixel/voxel
	'''

	arr = addSkinFat(arr)

	s_params = np.zeros(arr.shape+(4,),dtype='float')
	for c in np.unique(arr):
		s_params += (arr[...,None]==c) * getPropertiesFromClass(c, params_to_use)

	return s_params

def getPropertiesFromClass(c, params_to_use='ours'):
	'''
	returns the PD,T1 and T2* values for a given tissue, all normalized to the range [0,1]
		values drawn from:
		- https://radiologykey.com/magnetic-resonance-imaging-tissue-parameters/
		- vti
	'''

	if params_to_use == 'ours':
		pd_max, t1_max, t2_max, t2star_max = 90.,1400.,285.,70.
		muscle = 	np.array([88./pd_max, 1000./t1_max, 43./t2_max,  28./t2star_max])
		blood = 	np.array([79./pd_max, 1387./t1_max, 280./t2_max, 66./t2star_max])
		liver = 	np.array([87./pd_max, 661./t1_max,  57./t2_max,  34./t2star_max])
		bone = 		np.array([71./pd_max, 250./t1_max,  20./t2_max,  1./t2star_max])
		fat = 		np.array([60./pd_max, 250./t1_max,  70./t2_max,  39./t2star_max])
		average = 	np.array([65./pd_max, 750./t1_max,  60/t2_max,   30./t2star_max])
		air = 		np.array([34./pd_max, 1171./t1_max, 61./t2_max,  1./t2star_max])

	if params_to_use == 'mrxcat':
		pd_max, t1_max, t2_max, t2star_max = 100.,1300.,105.,55.
		muscle = 	np.array([80./pd_max, 900./t1_max,  50./t2_max,  31./t2star_max])
		blood = 	np.array([95./pd_max, 1200./t1_max, 100./t2_max, 50./t2star_max])
		liver = 	np.array([90./pd_max, 800./t1_max,  50./t2_max,  31./t2star_max])
		bone = 		np.array([12./pd_max, 250./t1_max,  20./t2_max,  1./t2star_max])
		fat = 		np.array([70./pd_max, 350./t1_max,  30./t2_max,  20./t2star_max])
		average = 	np.array([70./pd_max, 700./t1_max,  50/t2_max,   30./t2star_max])
		air = 		np.array([34./pd_max, 1171./t1_max, 61./t2_max,  1./t2star_max])

	if c in [0,15,16,52]:
		return air
	elif c in [1,2,3,4,10]:
		return muscle
	elif c in [5,6,7,8,36,37]:
		return blood
	elif c in [31,32,33,34,35,51]:
		return bone
	elif c in [50,99]:
		return fat
	elif c in [13,40,41,42,43]:
		return liver
	else:
		return average
"""

def simpleSignalModel(arr, TR=4, TE=2, flip_angle=0.35, mode='bssfp'):

	# TR=3, TE=1.5, flip_angle = 1.04

	'''
	simple analytic signal estimation from PD, T1 and T2*
	returns a "grayscale" intensity image
	'''

	PD, T1, T2, T2_star = arr[...,0]*100., arr[...,1]*1400., arr[...,2]*285., arr[...,3]*70.

	if mode.lower() == 'gre':
		c1 = np.exp(-TR/T1)
		c2 = np.exp(-TE/T2_star)
		c3 = (1-c1)*np.sin(flip_angle)*c2
		c4 = np.cos(flip_angle)*c1
		m =  PD*c3/c4

		m = m * 2.9

		return m

	elif mode.lower() == 'bssfp':
		theta, tau = 0, 0.5

		T2_ = T2*T2_star/(T2-T2_star)
		e1 = np.exp(-TR/T1)
		e2 = np.exp(-TR/T2)
		e2_ = np.exp(-TR/T2_)
		a = 1-np.cos(flip_angle)
		b = np.cos(flip_angle)-e1
		c1 = (a-e2**2*b-np.sqrt((a**2-e2**2*b**2)*(1-e2**2)))/(a-b)
		c2 = (e2*(a+b)/2)/(a-c1*(a-b)/2)
		m0 = -1j*np.sin(flip_angle)*(1-e1)/(a+b*c1)
		m = e2**tau * np.exp(1j*tau*theta)
		m = m * ((e2_**tau)/(1+np.exp(1j*theta)*e2_*c2) + (e2_**(1-tau)*np.exp(1j*theta)*c1)/(e2*(1+np.exp(1j*theta)*e2_*c2))) * m0 * PD

		m = np.abs(m)

		m = m * 0.046

		return m

def SignalCheck(PD,T1,T2,T2_star, TR=4, TE=2, flip_angle=0.35, mode='bssfp'):

	# TR=3, TE=1.5, flip_angle = 1.04

	'''
	simple analytic signal estimation from PD, T1 and T2*
	returns a "grayscale" intensity image
	'''
	theta, tau = 0, 0.5
	
	T2_ = T2*T2_star/(T2-T2_star)
	e1 = np.exp(-TR/T1)
	e2 = np.exp(-TR/T2)
	e2_ = np.exp(-TR/T2_)
	a = 1-np.cos(flip_angle)
	b = np.cos(flip_angle)-e1
	c1 = (a-e2**2*b-np.sqrt((a**2-e2**2*b**2)*(1-e2**2)))/(a-b)
	c2 = (e2*(a+b)/2)/(a-c1*(a-b)/2)
	m0 = -1j*np.sin(flip_angle)*(1-e1)/(a+b*c1)
	m = e2**tau * np.exp(1j*tau*theta)
	m = m * ((e2_**tau)/(1+np.exp(1j*theta)*e2_*c2) + (e2_**(1-tau)*np.exp(1j*theta)*c1)/(e2*(1+np.exp(1j*theta)*e2_*c2))) * m0 * PD

	m = np.abs(m)

	m = m * 0.046

	return m

# if __name__ == '__main__':

# 	xcat_anatomy = np.load('/home/tom/Desktop/projects/xcatanatomygenerator/synthetic_anatomy.npy')

# 	print(xcat_anatomy.shape)

# 	im = ((xcat_anatomy == 13)*1)[:,128]
# 	imageio.imwrite('image_variety.png', im)
# 	# sys.exit()

# 	pha = Phantom(xcat_anatomy[:,128])
# 	# pha = Phantom(xcat_anatomy[220])
	
# 	ims = []
# 	for ptu in ['xcat', 'ours']:
# 		img1 = pha.simpleSignalModel(property_values_to_use=ptu)
# 		img2 = pha.simpleSignalModel(TR=3, TE=1.5, flip_angle=1.04, property_values_to_use=ptu)
# 		img3 = pha.simpleSignalModel(sequence='gre', property_values_to_use=ptu)

# 		ims.append( np.concatenate([img1,img2,img3], axis=1) )

# 		print(img1.min(), img1.max())
# 		print(img2.min(), img2.max())
# 		print(img3.min(), img3.max())

# 	imageio.imwrite('image_variety.png', (np.concatenate(ims)*255).astype('uint8') )


