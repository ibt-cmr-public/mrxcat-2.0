import os
import vtk
from vtk.numpy_interface import dataset_adapter as dsa
import imageio
import numpy as np

from phantomModel import Phantom, simpleSignalModel, addSkinFat

import torch
device = 'cpu'#torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
from res_unet_model import ResUNet

class XCATProperties:
	def __init__(self,):
		self.muscle = 	np.array([80., 900.,  50.,  31.])
		self.fat = 		np.array([70., 350.,  30.,  20.])
		self.blood = 	np.array([95., 1200., 100., 50.])
		self.liver = 	np.array([90., 800.,  50.,  31.])
		self.bone = 	np.array([12., 250.,  20.,  1.])
		self.average = 	np.array([70., 700.,  50.,  30.])
		self.air = 		np.array([34., 1171., 61.,  1.])

class TrainingProperties:
	def __init__(self,):
		self.muscle  = 	np.array([88., 1000., 43.,  28.])
		self.fat     = 	np.array([60., 250.,  70.,  39.])
		self.blood   = 	np.array([79., 1387., 280., 66.])
		self.liver   = 	np.array([87., 661.,  57.,  34.])
		self.bone    = 	np.array([71., 250.,  20.,  1.])
		self.average = 	np.array([65., 750.,  60,   30.])
		self.air     =  np.array([34., 1171., 61.,  1.])
	

def loadVTI(fname):
	#load a VTI file:
	reader = vtk.vtkXMLImageDataReader()
	reader.SetFileName( fname )
	reader.Update()
	data = reader.GetOutput()
	rows, cols, slices = data.GetDimensions()
	lab = dsa.WrapDataObject(data).PointData['labels'].reshape(slices, cols, rows) #unsure if it should be ,rows,cols or ,cols,rows (not an issue for square images)
	return lab

def addTexture(tpm,net_path):

	from phantomModel import pd_max, t1_max, t2_max, t2star_max
	scale = np.array([[[[pd_max, t1_max, t2_max, t2star_max]]]])

	net = ResUNet(3,3).to(device)
	net.load_state_dict(torch.load(net_path,map_location=device))
	net.eval()

	tpm_tensor = torch.Tensor(np.moveaxis(tpm[...,:3],-1,1)).to(device)
	res =  np.moveaxis( net(tpm_tensor).to('cpu').detach().numpy(), 1, -1)
	normalized_textures = np.concatenate([res,tpm[...,-1:]], axis=-1) 

	textures = scale * normalized_textures
	
	return textures

def defineTissueProperties(vtiIMG,net_path):
	
	XCAT_val = XCATProperties()
	rows, cols, slices = vtiIMG.GetDimensions()
	lab = dsa.WrapDataObject(vtiIMG).PointData['labels'].reshape(slices, cols, rows) #unsure if it should be ,rows,cols or ,cols,rows (not an issue for square images)

	#lab = loadVTI(image)
	lab = np.moveaxis(lab,1,2) # this axis swap is to make the image orientation match the images Stefano has in the SAX_slices folder
	assert len(lab.shape) == 3

	textured_tpm = np.zeros((lab.shape[0],lab.shape[1],lab.shape[2],4))

#	lab = addSkinFat(lab) #optionally add a fat layer around the edge of the phantom
	
	for ii in range(textured_tpm.shape[0]):
		lab_sel = np.expand_dims(lab[ii,:,:],0)
		pha = Phantom(lab_sel) # convert to the format we use for the NN (basically rather than ~50 classes we convert to ~6 classes)

		#classes = pha.phantom #this is just an array with the reduuced classes, pottentially useful for visualisation
		tpm = pha.getTissuePropertyMap() #this gets the tissue property maps based on the pixel classes

		textured_sel = addTexture(tpm,net_path) #use the trained network to add some texture (un-normalized)
		textured_tpm[ii,:,:,:] = textured_sel


	# properties in shape out of the NN
	PD_NN   = textured_tpm[:,:,:,0]
	T1_NN   = textured_tpm[:,:,:,1]
	T2_NN   = textured_tpm[:,:,:,2]
	T2s_NN  = textured_tpm[:,:,:,3]

	# Reshaping with shapes needed for phantom images
	PD  = np.zeros((PD_NN.shape[1],PD_NN.shape[2],PD_NN.shape[0]))
	T1  = np.zeros((PD_NN.shape[1],PD_NN.shape[2],PD_NN.shape[0]))
	T2  = np.zeros((PD_NN.shape[1],PD_NN.shape[2],PD_NN.shape[0]))
	T2s  = np.zeros((PD_NN.shape[1],PD_NN.shape[2],PD_NN.shape[0]))

	for ii in range(PD_NN.shape[0]):
		PD[:,:,ii]  = PD_NN[ii,:,:]
		T1[:,:,ii]  = T1_NN[ii,:,:]
		T2[:,:,ii]  = T2_NN[ii,:,:]
		T2s[:,:,ii] = T2s_NN[ii,:,:]

	return PD,T1,T2,T2s

def normalizeTissuePropertieswXCAT(data_input,PD,T1,T2):

	import numpy as np
	XCAT_values = XCATProperties()

	labels = np.unique(data_input)
	
	for label_sel in labels:
		if label_sel in [1,2,3,4,10]:
			prop_tissue = XCAT_values.muscle
		elif label_sel in [5,6,7,8]:
			prop_tissue = XCAT_values.blood
		elif label_sel in [0,15,16]:
			prop_tissue = XCAT_values.air
		elif label_sel in [13,40,41,42,43,52]:
			prop_tissue = XCAT_values.liver
		elif label_sel in [50,99]:
			prop_tissue = XCAT_values.fat
		elif label_sel in [31,32,33,34,35,51]:
			prop_tissue = XCAT_values.bone
		else:
			prop_tissue = XCAT_values.average

		id_img = np.where(data_input == label_sel)
		mean_PD = np.mean(PD[id_img])
		if mean_PD  > 0:
			PD[id_img] *= prop_tissue[0]/mean_PD
		mean_T1 = np.mean(T1[id_img])
		if mean_T1  > 0:
			T1[id_img] *= prop_tissue[1]/mean_T1
		mean_T2 = np.mean(T2[id_img])
		if mean_T2  > 0:
			T2[id_img] *= prop_tissue[2]/mean_T2

	return PD,T1,T2

def fixLVTexture(data_input,PD,T1,T2,T2s,maskLabels,which_prop='Training'):

	import numpy as np
	if which_prop == 'XCAT':
		Fix_values = XCATProperties()
	elif which_prop == 'Training':
		Fix_values = TrainingProperties()
		
	id_img = np.where(data_input == maskLabels.LV_wall)		
	
	if which_prop == 'meanLV':
		PD_LV = np.mean(PD[id_img]) 
		T1_LV = np.mean(T1[id_img]) 
		T2_LV = np.mean(T2[id_img]) 
		T2s_LV = np.mean(T2s[id_img]) 
	else:
		PD_LV = Fix_values.muscle[0]
		T1_LV = Fix_values.muscle[1] 
		T2_LV = Fix_values.muscle[2]
		T2s_LV = Fix_values.muscle[3]

	PD[id_img] = PD_LV
	T1[id_img] = T1_LV
	T2[id_img] = T2_LV
	T2s[id_img] = T2s_LV

	return PD,T1,T2,T2s,[PD_LV,T1_LV,T2_LV,T2s_LV]


def defineTissuePropertiesMRXCAT(data_input):

	import numpy as np
	PD = np.zeros(data_input.shape)
	T1 = np.zeros(data_input.shape)
	T2 = np.zeros(data_input.shape)

	XCAT_values = XCATProperties()

	labels = np.unique(data_input)

	for label_sel in labels:
		if label_sel in [1,2,3,4,10]:
			prop_tissue = XCAT_values.muscle
			PD_LV  = prop_tissue[0]
			T1_LV  = prop_tissue[1]
			T2_LV  = prop_tissue[2]
			T2s_LV = 0
		elif label_sel in [5,6,7,8]:
			prop_tissue = XCAT_values.blood
		elif label_sel in [0,15,16]:
			prop_tissue = XCAT_values.air
		elif label_sel in [13,40,41,42,43,52]:
			prop_tissue = XCAT_values.liver
		elif label_sel in [50,99]:
			prop_tissue = XCAT_values.fat
		elif label_sel in [31,32,33,34,35,51]:
			prop_tissue = XCAT_values.bone
		else:
			prop_tissue = XCAT_values.average

		id_img = np.where(data_input == label_sel)
		PD[id_img] = prop_tissue[0]
		T1[id_img] = prop_tissue[1]
		T2[id_img] = prop_tissue[2]
		T2s = T2*0
	return PD,T1,T2,T2s,[PD_LV,T1_LV,T2_LV,T2s_LV]


def textureReport(ind_LV_wall,ind_LV_BP,PD,T1,T2,out_file):

	import matplotlib.pyplot as plt
	from scipy.stats import norm

	XCAT_values = XCATProperties()
	Training_values = TrainingProperties()

	tissue_property_values = {}

	# muscle = 	np.array([88., 1000., 43.,  28.])
	# fat = 		np.array([60., 250.,  70.,  39.])
	# blood = 	np.array([79., 1387., 280., 66.])
	# liver = 	np.array([87., 661.,  57.,  34.])
	# bone = 		np.array([71., 250.,  20.,  1.])
	# unknown = 	np.array([65., 750.,  60,   30.])
	# air = 		np.array([34., 1171., 61.,  1.])
	# tissue_property_values['ours'] = [muscle, blood, air, liver, fat, bone, unknown]

	# muscle = 	np.array([80., 900.,  50.,  31.])
	# fat = 		np.array([70., 350.,  30.,  20.])
	# blood = 	np.array([95., 1200., 100., 50.])
	# liver = 	np.array([90., 800.,  50.,  31.])
	# bone = 		np.array([12., 250.,  20.,  1.])
	# average = 	np.array([70., 700.,  50.,  30.])
	# air = 		np.array([34., 1171., 61.,  1.])
	# tissue_property_values['xcat'] = [muscle, blood, air, liver, fat, bone, unknown]

	PD_LV = PD[ind_LV_wall]
	T1_LV = T1[ind_LV_wall]
	T2_LV = T2[ind_LV_wall]

	PD_LVb = PD[ind_LV_BP]
	T1_LVb = T1[ind_LV_BP]
	T2_LVb = T2[ind_LV_BP]

	fig, ax = plt.subplots(2,3)
	mu_PD_LV,sigma_PD_LV = norm.fit(PD_LV)
	hist_values = ax[0,0].hist(PD_LV,bins=50,density=True,color='b',alpha=0.5)
	ax[0,0].plot(XCAT_values.muscle[0]*np.array([1,1]),[0,np.max(hist_values[0])],'k-.',label='XCAT')
	ax[0,0].plot(Training_values.muscle[0]*np.array([1,1]),[0,np.max(hist_values[0])],'r--',label='REF')
	xmin = hist_values[1].min(); xmax = hist_values[1].max()+10
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu_PD_LV, sigma_PD_LV)
	ax[0,0].plot(x, p, 'k', linewidth=2)
	ax[0,0].set_title('mu=%1.2f' %(mu_PD_LV))
	ax[0,0].legend()

	mu_PD_LV,sigma_PD_LV = norm.fit(T1_LV)
	hist_values = ax[0,1].hist(T1_LV,bins=50,density=True,color='b',alpha=0.5)
	ax[0,1].plot(XCAT_values.muscle[1]*np.array([1,1]),[0,np.max(hist_values[0])],'k-.',label='XCAT')
	ax[0,1].plot(Training_values.muscle[1]*np.array([1,1]),[0,np.max(hist_values[0])],'r--',label='REF')
	xmin = hist_values[1].min(); xmax = hist_values[1].max()+10
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu_PD_LV, sigma_PD_LV)
	ax[0,1].plot(x, p, 'k', linewidth=2)
	ax[0,1].set_title('mu=%1.2f' %(mu_PD_LV))
	ax[0,1].legend()

	mu_PD_LV,sigma_PD_LV = norm.fit(T2_LV)
	hist_values = ax[0,2].hist(T2_LV,bins=50,density=True,color='b',alpha=0.5)
	ax[0,2].plot(XCAT_values.muscle[2]*np.array([1,1]),[0,np.max(hist_values[0])],'k-.',label='XCAT')
	ax[0,2].plot(Training_values.muscle[2]*np.array([1,1]),[0,np.max(hist_values[0])],'r--',label='REF')
	xmin = hist_values[1].min(); xmax = hist_values[1].max()+10
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu_PD_LV, sigma_PD_LV)
	ax[0,2].plot(x, p, 'k', linewidth=2)
	ax[0,2].set_title('mu=%1.2f' %(mu_PD_LV))
	ax[0,2].legend()

	mu_PD_LV,sigma_PD_LV = norm.fit(PD_LVb)
	hist_values = ax[1,0].hist(PD_LVb,bins=50,density=True,color='b',alpha=0.5)
	ax[1,0].plot(XCAT_values.blood[0]*np.array([1,1]),[0,np.max(hist_values[0])],'k-.',label='XCAT')
	ax[1,0].plot(Training_values.blood[0]*np.array([1,1]),[0,np.max(hist_values[0])],'r--',label='REF')
	xmin = hist_values[1].min(); xmax = hist_values[1].max()+10
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu_PD_LV, sigma_PD_LV)
	ax[1,0].plot(x, p, 'k', linewidth=2)
	ax[1,0].set_title('mu=%1.2f' %(mu_PD_LV))
	ax[1,0].legend()

	mu_PD_LV,sigma_PD_LV = norm.fit(T1_LVb)
	hist_values = ax[1,1].hist(T1_LVb,bins=50,density=True,color='b',alpha=0.5)
	ax[1,1].plot(XCAT_values.blood[1]*np.array([1,1]),[0,np.max(hist_values[0])],'k-.',label='XCAT')
	ax[1,1].plot(Training_values.blood[1]*np.array([1,1]),[0,np.max(hist_values[0])],'r--',label='REF')
	xmin = hist_values[1].min(); xmax = hist_values[1].max()+10
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu_PD_LV, sigma_PD_LV)
	ax[1,1].plot(x, p, 'k', linewidth=2)
	ax[1,1].set_title('mu=%1.2f' %(mu_PD_LV))

	mu_PD_LV,sigma_PD_LV = norm.fit(T2_LVb)
	hist_values = ax[1,2].hist(T2_LVb,bins=50,density=True,color='b',alpha=0.5)
	ax[1,2].plot(XCAT_values.blood[2]*np.array([1,1]),[0,np.max(hist_values[0])],'k-.',label='XCAT')
	ax[1,2].plot(Training_values.blood[2]*np.array([1,1]),[0,np.max(hist_values[0])],'r--',label='REF')
	xmin = hist_values[1].min(); xmax = hist_values[1].max()+10
	x = np.linspace(xmin, xmax, 100)
	p = norm.pdf(x, mu_PD_LV, sigma_PD_LV)
	ax[1,2].plot(x, p, 'k', linewidth=2)
	ax[1,2].set_title('mu=%1.2f' %(mu_PD_LV))
	plt.tight_layout()
	plt.savefig(out_file,dpi=400)
	plt.close()

	return 0



